class DateFormatter {

  static String getFormattedDate(DateTime date) {
    String formattedDate, formattedMonth;

    switch (date.month) {
      case 1:
        formattedMonth = 'ENE';
        break;
      case 2:
        formattedMonth = 'FEB';
        break;
      case 3:
        formattedMonth = 'MAR';
        break;
      case 4:
        formattedMonth = 'ABR';
        break;
      case 5:
        formattedMonth = 'MAY';
        break;
      case 6:
        formattedMonth = 'JUN';
        break;
      case 7:
        formattedMonth = 'JUL';
        break;
      case 8:
        formattedMonth = 'AGO';
        break;
      case 9:
        formattedMonth = 'SEP';
        break;
      case 10:
        formattedMonth = 'OCT';
        break;
      case 11:
        formattedMonth = 'NOV';
        break;
      case 12:
        formattedMonth = 'DIC';
        break;
    }
    formattedDate = '${date.year}-$formattedMonth-${date.day}';

    return formattedDate;
  }

  static String convertDateTimeDisplay(DateTime date) {
    String month = date.month < 10 ? '0${date.month}' : '${date.month}';
    String day = date.day < 10 ? '0${date.day}' : '${date.day}';
    final String formatted = '${date.year}-$month-$day';
    return formatted;
  }

}
