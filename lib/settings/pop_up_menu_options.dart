class PopUpMenuOptions {
  static const String login = "Iniciar sesión";
  static const String about = "Acerca de Firulife";

  static const List<String> options = [
    login,
    about
  ];

}
