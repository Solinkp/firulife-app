class FiruAssets {
  static const firuLogoWhite = 'assets/images/firu_logotipo_white.svg';
  static const firuLogoRoundFull = 'assets/images/firu_logotipo_round_full.svg';
  static const firuShelter = 'assets/images/firu_shelter.svg';
  static const firuStore = 'assets/images/firu_store.svg';
  static const firuVet = 'assets/images/firu_vet.svg';
  static const firuLostAndFound = 'assets/images/firu_lost_found.svg';
  static const firuWalk = 'assets/images/firu_walk.svg';
}
