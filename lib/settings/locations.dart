class Locations {
  static const COUNTRIES = {
    'NIC': 'Nicaragua',
    'SLV': 'El Salvador'
  };

  static const NICKEY = 'NIC';
  static const SLVKEY = 'SLV';

  static const NICPHONECODE = '+505';
  static const SLVPHONECODE = '+503';

  static const NICDEPTS = [
    'Seleccione un Departamento',
    'Boaco',
    'Chinandega',
    'Estelí',
    'Granada',
    'Jinotega',
    'Carazo',
    'Chontales',
    'León',
    'Madriz',
    'Managua',
    'Masaya',
    'Matagalpa',
    'Nueva Segovia',
    'Costa Caribe Sur',
    'Costa Caribe Norte',
    'Rivas',
    'Río San Juan'
  ];

  static const SLVDEPTS = [
    'Seleccione un Departamento',
    'Ahuachapán',
    'Cabañas',
    'Chalatenango',
    'Cuscatlán',
    'La Libertad',
    'La Paz',
    'La Unión',
    'Morazán',
    'San Miguel',
    'San Salvador',
    'San Vicente',
    'Santa Ana',
    'Sonsonate',
    'Usulután'
  ];

}
