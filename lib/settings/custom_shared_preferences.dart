import 'package:shared_preferences/shared_preferences.dart';

class CustomSharedPreferences {
  final String _prefUserEmail = "email";
  final String _prefUserCountry = "country";
  final String _prefUserDepartment = "dept";

  Future<String> getLocalEmail() async {
    final SharedPreferences _prefs = await SharedPreferences.getInstance();
    return _prefs.getString(_prefUserEmail) ?? '';
  }

  Future<String> getLocalCountry() async {
    final SharedPreferences _prefs = await SharedPreferences.getInstance();
    return _prefs.getString(_prefUserCountry) ?? '';
  }

  Future<String> getLocalDepartment() async {
    final SharedPreferences _prefs = await SharedPreferences.getInstance();
    return _prefs.getString(_prefUserDepartment) ?? '';
  }

  Future<bool> setLocalEmail(String value) async {
    final SharedPreferences _prefs = await SharedPreferences.getInstance();
    return _prefs.setString(_prefUserEmail, value);
  }

  Future<bool> setLocalCountry(String value) async {
    final SharedPreferences _prefs = await SharedPreferences.getInstance();
    return _prefs.setString(_prefUserCountry, value);
  }

  Future<bool> setLocalDepartment(String value) async {
    final SharedPreferences _prefs = await SharedPreferences.getInstance();
    return _prefs.setString(_prefUserDepartment, value);
  }

}
