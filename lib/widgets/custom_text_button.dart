import 'package:flutter/material.dart';

import '../settings/custom_colors.dart';

class CustomTextButton extends StatelessWidget {
  final String text;
  final Function pressCallback;

  CustomTextButton({
    @required this.text,
    @required this.pressCallback
  });

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: pressCallback,
      child: Text(text, style: TextStyle(color: CustomColors.secondaryGreen)),
      style: TextButton.styleFrom(
        primary: CustomColors.secondaryGreen
      )
    );
  }

}
