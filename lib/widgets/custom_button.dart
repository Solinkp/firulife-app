import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  final double topPadding;
  final double botPadding;
  final double height;
  final double elevation;
  final Color buttonColor;
  final String text;
  final double textSize;
  final Color textColor;
  final Function pressCallback;

  CustomButton({
    @required this.topPadding,
    @required this.botPadding,
    @required this.buttonColor,
    @required this.text,
    @required this.textSize,
    @required this.pressCallback,
    this.textColor = Colors.white,
    this.height = 40.0,
    this.elevation = 4.0,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: topPadding, bottom: botPadding),
      child: SizedBox(
        height: height,
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            elevation: elevation,
            primary: buttonColor,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30.0)
            ),
            textStyle: TextStyle(fontSize: textSize, color: textColor)
          ),
          child: Text(text),
          onPressed: pressCallback
        )
      )
    );
  }

}
