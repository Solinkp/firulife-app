import 'package:flutter/material.dart';

import '../routing/fade_route.dart';
import '../models/pet.dart';
import '../settings/placeholder_images.dart';
import '../settings/custom_colors.dart';
import '../screens/business/details/shelter_pet_details_screen.dart';

class ShelterPet extends StatelessWidget {
  final Pet pet;
  final Color businessColor;

  ShelterPet({
    @required this.pet,
    @required this.businessColor
  });

  @override
  Widget build(BuildContext context) {
    String petPicture = '';

    if(pet.pictureUrl != null && pet.pictureUrl.length > 0) {
      petPicture = pet.pictureUrl;
    }

    return InkWell(
      highlightColor: Colors.transparent,
      splashColor: Colors.transparent,
      onTap: () => _goToPetDetails(context),
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Hero(
              tag: 'firu_${pet.id}',
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100),
                  border: Border.all(color: businessColor ?? CustomColors.mainGreen, width: 2)
                ),
                child: ClipRRect(
                  borderRadius: const BorderRadius.all(Radius.circular(100)),
                  child: FadeInImage(
                    height: 100,
                    width: 100,
                    fit: BoxFit.cover,
                    placeholder: const AssetImage(PlaceholderImages.placeholderFiru),
                    image: petPicture.isEmpty ? const AssetImage(PlaceholderImages.placeholderFiru) : NetworkImage(petPicture),
                    imageErrorBuilder: (BuildContext context, Object exception, StackTrace stackTrace) {
                      return Image.asset(PlaceholderImages.placeholderFiru);
                    }
                  )
                )
              )
            ),
            Container(
              alignment: Alignment.center,
              child: Text(pet.name, style: const TextStyle(fontSize: 17, color: CustomColors.mainDark, fontWeight: FontWeight.bold))
            )
          ]
        )
      )
    );
  }

  void _goToPetDetails(BuildContext context) {
    Navigator.of(context).push(FadeRoute(page: ShelterPetDetailsScreen(), arguments: [pet, businessColor]));
  }

}
