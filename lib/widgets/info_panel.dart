import 'package:flutter/material.dart';

import '../settings/custom_colors.dart';
import './contact_icon.dart';

class InfoPanel extends StatelessWidget {
  final String title;
  final List list;
  final Map map;
  final int type;
  final Color personalizedColor;

  InfoPanel({
    @required this.title,
    @required this.type,
    @required this.personalizedColor,
    this.list,
    this.map
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 5),
        child: ExpansionTile(
          title: Center(child: Text(title, style: const TextStyle(fontSize: 17, color: CustomColors.mainDark, fontWeight: FontWeight.bold))),
          children: _getChildren(context)
        )
      )
    );
  }

  List<Widget> _getChildren(BuildContext context) {
    List<Widget> children = [];
    List<Widget> childrenValue;
    switch(type) {
      case 0:
        list.forEach((daysSchedule) => {
          children.add(
            Row(children: [
              Expanded(
                flex: 2,
                child: Text(
                  '${daysSchedule['day']}:',
                  textAlign: TextAlign.center,
                  style: const TextStyle(color: CustomColors.mainDark, fontSize: 16)
                )
              ),
              Expanded(
                flex: 5,
                child: ListTile(
                  title: Text(
                    daysSchedule['schedule'],
                    style: const TextStyle(color: CustomColors.mainDark)
                  )
                )
              )
            ])
          )
        });
        break;
      case 1:
        list.forEach((element) {
          children.add(
            ListTile(
              title: Text('- $element', style: const TextStyle(color: CustomColors.mainDark))
            )
          );
        });
        break;
      case 2:
        map.forEach((key, value) => {
          childrenValue = [],
          value.forEach((element) => {
            childrenValue.add(
              Row(children: <Widget>[
                Expanded(
                  flex: 2,
                  child: Text(
                    '${element.keys.first}:',
                    textAlign: TextAlign.center,
                    style: const TextStyle(color: CustomColors.mainDark, fontSize: 16)
                  )
                ),
                Expanded(
                  flex: 3,
                  child: ListTile(
                    title: SelectableText(
                      element.values.first,
                      textAlign: TextAlign.start,
                      style: const TextStyle(color: CustomColors.mainDark)
                    )
                  )
                )
              ])
            )
          }),
          children.add(
            ExpansionTile(
              title: Text(key, style: const TextStyle(fontSize: 15, color: CustomColors.mainDark)),
              children: childrenValue
            )
          )
        });
        break;
      case 3:
        childrenValue = [];
        list.forEach((element) {
          element.forEach((key, value) => {
            childrenValue.add(
              ContactIcon(contact: key, url: value, personalizedColor: personalizedColor, isPhone: false)
            )
          });
        });
        children.add(
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(children: childrenValue)
          )
        );
        break;
    }
    return children;
  }

}
