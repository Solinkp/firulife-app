import 'package:flutter/material.dart';

class FormErrorMessage extends StatelessWidget {
  final String errorMessage;
  final double topPadding;
  final Color textColor;

  FormErrorMessage({
    @required this.errorMessage,
    @required this.topPadding,
    @required this.textColor
  });

  @override
  Widget build(BuildContext context) {
    if(errorMessage != null && errorMessage.length > 0) {
      return Container(
        padding: EdgeInsets.symmetric(vertical: topPadding),
        child: Text(
          errorMessage,
          style: TextStyle(
            fontSize: 14.0,
            color: textColor,
            height: 1.0,
            fontWeight: FontWeight.w300
          )
        )
      );
    } else {
      return Container(
        height: 0.0,
        width: 0.0
      );
    }
  }

}
