import 'package:flutter/material.dart';

import '../routing/fade_route.dart';
import '../models/product.dart';
import '../settings/placeholder_images.dart';
import '../settings/custom_colors.dart';
import '../screens/business/details/business_product_details_screen.dart';

class BusinessProduct extends StatelessWidget {
  final Product product;
  final Color businessColor;

  BusinessProduct({
    @required this.product,
    @required this.businessColor
  });

  @override
  Widget build(BuildContext context) {
    String productPicture = '';

    if(product.pictureUrl != null && product.pictureUrl.length > 0) {
      productPicture = product.pictureUrl;
    }

    return Column(children: <Widget>[
      InkWell(
        onTap: () => _goToProductDetails(context),
        child: ListTile(
          leading: Hero(
            tag: 'product_${product.id}',
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                border: Border.all(color: businessColor ?? CustomColors.mainGreen, width: 2)
              ),
              child: ClipRRect(
                borderRadius: const BorderRadius.all(Radius.circular(13)),
                child: FadeInImage(
                  fit: BoxFit.fitWidth,
                  placeholder: const AssetImage(PlaceholderImages.placeholderProduct),
                  image: productPicture.isEmpty ? AssetImage(PlaceholderImages.placeholderProduct) : NetworkImage(productPicture),
                  imageErrorBuilder: (BuildContext context, Object exception, StackTrace stackTrace) {
                    return Image.asset(PlaceholderImages.placeholderProduct);
                  }
                )
              )
            )
          ),
          title: Text(product.name, style: TextStyle(fontSize: 17, color: CustomColors.mainDark)),
          subtitle: Text(product.smallDescription, style: const TextStyle(fontSize: 14)),
          trailing: Text('${product.currency}${product.price}', style: TextStyle(fontSize: 17, color: CustomColors.mainDark))
        )
      ),
      Divider()
    ]);
  }

  void _goToProductDetails(BuildContext context) {
    Navigator.of(context).push(FadeRoute(page: BusinessProductDetailsScreen(), arguments: [product, businessColor]));
  }

}
