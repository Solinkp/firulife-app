import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:io';

import '../settings/custom_colors.dart';

class ContactIcon extends StatelessWidget {
  final String contact;
  final String url;
  final Color personalizedColor;
  final bool isPhone;
  final double iconSize;
  static String newUrl;

  ContactIcon({
    @required this.contact,
    @required this.url,
    @required this.personalizedColor,
    @required this.isPhone,
    this.iconSize = 42.0
  });

  @override
  Widget build(BuildContext context) {
    return IconButton(
      iconSize: iconSize,
      color: personalizedColor ?? CustomColors.mainGreen,
      icon: _getIcon(),
      onPressed: () => isPhone ? _goToContact() : _goToSocial()
    );
  }

  Widget _getIcon() {
    Icon icon;
    switch(contact) {
      case 'email':
        icon = Icon(MdiIcons.emailOutline);
        newUrl = 'mailto:' + url;
        break;
      case 'web':
        icon = Icon(MdiIcons.web);
        break;
      case 'facebook':
        icon = Icon(MdiIcons.facebook);
        break;
      case 'instagram':
        icon = Icon(MdiIcons.instagram);
        break;
      case 'twitter':
        icon = Icon(MdiIcons.twitter);
        break;
      case 'youtube':
        icon = Icon(MdiIcons.youtube);
        break;
      case 'whatsapp':
        icon = Icon(MdiIcons.whatsapp);
        break;
    }
    return icon;
  }

  void _goToSocial() async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      if (await canLaunch(newUrl)) {
        await launch(newUrl);
      }
    }
  }

  void _goToContact() async {
    String link;
    if(Platform.isIOS) {
      link = 'whatsapp://wa.me/$url';
    } else {
      link = 'whatsapp://send?phone=$url';
    }
    if(await canLaunch(link)) {
      await launch(link);
    }
  }

}
