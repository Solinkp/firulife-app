import 'package:flutter/material.dart';

import '../settings/custom_shared_preferences.dart';
import '../screens/loading_screen.dart';
import '../screens/country_screen.dart';
import '../screens/home/home_screen.dart';

class RootWidget extends StatefulWidget {

  @override
  _RootWidgetState createState() => _RootWidgetState();
}

class _RootWidgetState extends State<RootWidget> {
  String _country;
  bool _isLoading = true;

  @override
  void initState() {
    CustomSharedPreferences().getLocalCountry().then((value) {
      setState(() {
        _country = value;
        _isLoading = false;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    if(_isLoading){
      return LoadingScreen();
    } else {
      if(_country.isEmpty) {
        return CountryScreen();
      } else {
        return HomeScreen();
      }
    }
  }

}
