import 'package:flutter/material.dart';
import 'package:carousel_pro/carousel_pro.dart';

import '../routing/fade_route.dart';
import '../models/lost_and_found_post.dart';
import '../settings/custom_colors.dart';
import '../settings/placeholder_images.dart';
import '../screens/lost_found/lf_post_details_screen.dart';

class LostAndFoundPostCard extends StatelessWidget {
  final LostAndFoundPost post;

  LostAndFoundPostCard({
    @required this.post
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      key: Key(post.id),
      alignment: Alignment.center,
      margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      child: InkWell(
        borderRadius: BorderRadius.circular(15),
        onTap: () => Navigator.of(context).push(FadeRoute(page: LostAndFoundPostDetailsScreen(), arguments: post)),
        child: Card(
          color: CustomColors.mainGreen,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15),
            side: BorderSide(color: CustomColors.secondaryGreen, width: 1.0)
          ),
          elevation: 5,
          child: Column(children: [
            ClipRRect(
              borderRadius: const BorderRadius.only(
                topLeft: const Radius.circular(15),
                topRight: const Radius.circular(15)
              ),
              child: Container(
                color: Colors.white12,
                alignment: Alignment.center,
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 6),
                  child: Text(post.lost ? 'SE BUSCA' : 'EXTRAVIADO', style: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
                )
              )
            ),
            SizedBox(
              height: 170.0,
              width: double.infinity,
              child: Carousel(
                autoplay: false,
                animationCurve: Curves.fastOutSlowIn,
                animationDuration: const Duration(milliseconds: 1000),
                dotSize: 4.0,
                dotIncreasedColor: CustomColors.secondaryGreen,
                dotColor: Colors.white,
                dotBgColor: Colors.transparent,
                dotPosition: DotPosition.bottomCenter,
                dotVerticalPadding: 5.0,
                showIndicator: post.pictures.length > 1 ? true : false,
                indicatorBgPadding: 0.0,
                images: post.pictures.map((picture) => Container(
                  padding: const EdgeInsets.symmetric(horizontal: 3.0),
                  child: FadeInImage(
                    fit: BoxFit.cover,
                    placeholder: const AssetImage(PlaceholderImages.placeholderFiru),
                    image: NetworkImage(picture),
                    imageErrorBuilder: (BuildContext context, Object exception, StackTrace stackTrace) {
                      return Image.asset(PlaceholderImages.placeholderFiru);
                    }
                  )
                )).toList()
              )
            ),
            ClipRRect(
              borderRadius: const BorderRadius.only(
                bottomLeft: const Radius.circular(15),
                bottomRight: const Radius.circular(15)
              ),
              child: Container(
                color: Colors.white12,
                alignment: Alignment.center,
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 6.0),
                  child: Text(
                    post.name.isNotEmpty ? post.name : 'Firu Encontrado',
                    style: Theme.of(context).textTheme.bodyText2,
                    textAlign: TextAlign.center
                  )
                )
              )
            )
          ])
        )
      )
    );
  }

}
