import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../routing/fade_route.dart';
import '../settings/custom_colors.dart';
import '../screens/business/businesses_screen.dart';
import '../screens/lost_found/lf_timeline_screen.dart';

class FiruSegment extends StatelessWidget {
  final int segmentId;
  final String segmentName;
  final String segmentImage;

  FiruSegment({
    @required this.segmentId,
    @required this.segmentName,
    @required this.segmentImage
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => _selectFiruSegment(context),
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(100.0),
        topRight: Radius.circular(100.0),
        bottomLeft: Radius.circular(20.0),
        bottomRight: Radius.circular(20.0)
      ),
      child: Container(
        decoration: BoxDecoration(
          color: CustomColors.secondaryGreen,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(100.0),
            topRight: Radius.circular(100.0),
            bottomLeft: Radius.circular(20.0),
            bottomRight: Radius.circular(20.0)
          )
        ),
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            SvgPicture.asset(
              segmentImage,
              width: 110,
              semanticsLabel: 'Icon for Firu Segment'
            ),
            Container(
              child: Padding(
                key: Key(segmentId.toString()),
                padding: EdgeInsets.symmetric(vertical: 7),
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(segmentName, style: Theme.of(context).textTheme.subtitle2)
                )
              )
            )
          ]
        )
      )
    );
  }

  void _selectFiruSegment(BuildContext context) {
    switch (segmentId) {
      case 0:
        // Shelters
        Navigator.of(context).push(FadeRoute(page: BusinessesScreen(), arguments: 0));
        break;
      case 1:
        // Vets
        _showMedicalSegmentSelection(context);
        break;
      case 2:
        // Stores
        Navigator.of(context).push(FadeRoute(page: BusinessesScreen(), arguments: 2));
        break;
      case 3:
        // Lost & Found
        Navigator.of(context).push(FadeRoute(page: LostAndFoundTimelineScreen()));
        break;
      case 4:
        // Walks
        // Navigator.of(context).push(FadeRoute(page: BusinessesScreen(), arguments: 3));
        break;
    }
  }

  void _showMedicalSegmentSelection(BuildContext context) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.mainDark,
          contentPadding: EdgeInsets.only(bottom: 15),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Stack(children: [
                Container(
                  alignment: Alignment.topRight,
                  child: IconButton(
                    color: CustomColors.secondaryGreen,
                    iconSize: 25,
                    icon: Icon(MdiIcons.closeCircleOutline),
                    onPressed: () {
                      Navigator.of(context).pop();
                    }
                  )
                ),
                Container(
                  padding: EdgeInsets.only(top: 25.0),
                  alignment: Alignment.bottomCenter,
                  child: Text('¿Qué deseas buscar?', style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.bold))
                )
              ]),
              Divider(),
              Row(children: [
                Expanded(
                  child: InkWell(
                    onTap: () {
                      Navigator.of(context).pop();
                      Navigator.of(context).push(FadeRoute(page: BusinessesScreen(), arguments: 1));
                    },
                    child: Column(children: [
                      Icon(
                        MdiIcons.hospitalBuilding,
                        size: 36,
                        color: CustomColors.secondaryGreen
                      ),
                      Text('Clínicas\nVeterinarias', textAlign: TextAlign.center, style: TextStyle(fontSize: 15, color: Colors.white))
                    ])
                  )
                ),
                Expanded(
                  child: InkWell(
                    onTap: () {
                      Navigator.of(context).pop();
                      Navigator.of(context).push(FadeRoute(page: BusinessesScreen(), arguments: 4));
                    },
                    child: Column(children: [
                      Icon(
                        MdiIcons.medicalBag,
                        size: 36,
                        color: CustomColors.secondaryGreen
                      ),
                      Text('Veterinarios\nIndependientes', textAlign: TextAlign.center, style: TextStyle(fontSize: 15, color: Colors.white))
                    ])
                  )
                )
              ])
            ]
          )
        );
      }
    );
  }

}
