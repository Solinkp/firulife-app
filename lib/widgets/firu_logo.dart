import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class FiruLogo extends StatelessWidget {
  final Color color;
  final double radius;
  final String imageAsset;

  FiruLogo({
    @required this.color,
    @required this.radius,
    @required this.imageAsset
  });

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: 'hero_firu_logo',
      child: Padding(
        padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
        child: CircleAvatar(
          backgroundColor: color,
          radius: radius,
          child: SvgPicture.asset(imageAsset)
        )
      )
    );
  }

}
