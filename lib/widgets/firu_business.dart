import 'package:flutter/material.dart';

import '../routing/fade_route.dart';
import '../models/business.dart';
import '../settings/custom_colors.dart';
import '../settings/placeholder_images.dart';
import '../screens/business/details/business_screen.dart';

class FiruBusiness extends StatelessWidget {
  final Business business;

  FiruBusiness({
    @required this.business
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      margin: const EdgeInsets.symmetric(vertical: 10),
      child: InkWell(
        borderRadius: BorderRadius.circular(20),
        onTap: () => _cardTapped(context),
        child: Card(
          color: business.businessColor != null ? business.businessColor : CustomColors.mainGreen,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
            side: BorderSide(color: CustomColors.secondaryGreen, width: 1.0)
          ),
          elevation: 4,
          child: Column(children: <Widget>[
            ClipRRect(
              borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(20),
                topRight: Radius.circular(20)
              ),
              child: FadeInImage(
                height: 160,
                width: double.infinity,
                fit: BoxFit.contain,
                placeholder: const AssetImage(PlaceholderImages.placeholderBanner),
                image: business.pictureUrl == null || business.pictureUrl.isEmpty ? const AssetImage(PlaceholderImages.placeholderBanner) : NetworkImage(business.pictureUrl),
                imageErrorBuilder: (BuildContext context, Object exception, StackTrace stackTrace) {
                  return Image.asset(PlaceholderImages.placeholderBanner);
                }
              )
            ),
            ClipRRect(
              borderRadius: const BorderRadius.only(
                bottomLeft: Radius.circular(20),
                bottomRight: Radius.circular(20)
              ),
              child: Container(
                width: double.infinity,
                alignment: Alignment.center,
                color: Colors.white70,
                child: Padding(
                  key: Key(business.id),
                  padding: EdgeInsets.all(10),
                  child: Text(
                    business.name,
                    style: const TextStyle(fontSize: 15, color: CustomColors.mainDark, fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center
                  )
                )
              )
            )
          ])
        )
      )
    );
  }

  void _cardTapped(context) {
    Navigator.of(context).push(FadeRoute(page: BusinessScreen(), arguments: business));
  }

}
