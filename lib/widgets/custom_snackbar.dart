import 'package:flutter/material.dart';

import '../settings/custom_colors.dart';

class CustomSnackbar extends StatelessWidget {
  final String message;
  final VoidCallback hideSnack;

  CustomSnackbar({
    @required this.message,
    @required this.hideSnack
  });

  @override
  Widget build(BuildContext context) {
    return SnackBar(
      backgroundColor: CustomColors.mainDark,
      content: Text(message),
      action: SnackBarAction(
        label: 'X',
        textColor: Colors.white,
        onPressed: () => hideSnack()
      )
    );
  }

}
