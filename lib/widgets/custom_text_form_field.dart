import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../settings/custom_colors.dart';

class CustomTextFormField extends StatefulWidget {
  final TextInputType inputType;
  final bool obscured;
  final String hintText;
  final IconData icon;
  final int index;
  final bool last;
  final Function(int, String) setValueForm;
  final Function(int, dynamic) validatorFunction;
  final String initialValue;
  final TextEditingController textController;
  final bool theme;
  final int lines;
  final int charLength;

  CustomTextFormField({
    @required this.inputType,
    @required this.obscured,
    @required this.hintText,
    @required this.icon,
    @required this.index,
    @required this.last,
    @required this.setValueForm,
    @required this.validatorFunction,
    this.initialValue,
    this.textController,
    this.theme = true,
    this.lines = 1,
    this.charLength
  });

  @override
  _CustomTextFormFieldState createState() => _CustomTextFormFieldState();
}

class _CustomTextFormFieldState extends State<CustomTextFormField> {
  bool _passwordInvisible = true;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: widget.index  == 0 ? const EdgeInsets.all(0.0) : const EdgeInsets.only(top: 15.0),
      child: TextFormField(
        maxLines: widget.lines,
        maxLength: widget.charLength,
        keyboardType: widget.inputType,
        obscureText: widget.obscured ? _passwordInvisible : widget.obscured,
        autofocus: false,
        cursorColor: widget.theme ? Colors.white : CustomColors.mainDark,
        decoration: InputDecoration(
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: widget.theme ? Colors.white : CustomColors.mainGreen)
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(15)),
            borderSide: BorderSide(color: widget.theme ? CustomColors.secondaryGreen : CustomColors.mainGreen)
          ),
          errorBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: widget.theme ? CustomColors.secondaryGreen : CustomColors.mainDark)
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(15)),
            borderSide: BorderSide(color: widget.theme ? CustomColors.secondaryGreen : CustomColors.mainGreen)
          ),
          errorStyle: TextStyle(color: widget.theme ? CustomColors.secondaryGreen : CustomColors.mainDark),
          hintText: widget.hintText,
          hintStyle: TextStyle(color: widget.theme ? Colors.white : CustomColors.mainDark),
          icon: Icon(
            widget.icon,
            color: widget.theme ? Colors.white : CustomColors.mainDark
          ),
          suffixIcon: widget.obscured ? IconButton(
            icon: Icon(
              _passwordInvisible ? Icons.visibility_off_outlined : Icons.visibility_outlined,
              color: widget.theme ? Colors.white : CustomColors.mainDark
            ),
            onPressed: () => {
              setState(() {
                _passwordInvisible = !_passwordInvisible;
              })
            }
          ) : null
        ),
        style: TextStyle(color: widget.theme ? Colors.white : CustomColors.mainDark),
        initialValue: widget.initialValue,
        controller: widget.textController,
        validator: (value) => widget.validatorFunction(widget.index, value),
        onSaved: (value) => widget.setValueForm(widget.index, value),
        textInputAction: widget.last ? TextInputAction.done : TextInputAction.next
      )
    );
  }

}
