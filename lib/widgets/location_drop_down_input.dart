import 'package:flutter/material.dart';

import '../settings/custom_colors.dart';

class LocationDropDownInput extends StatefulWidget {
  final int index;
  final String value;
  final String label;
  final IconData icon;
  final List<DropdownMenuItem<String>> getItems;
  final Function(int, dynamic) validatorFunction;
  final Function(int, String) setOnChangedValueForm;
  final Function(int, String) setOnSavedValueForm;
  final bool theme;

  LocationDropDownInput({
    @required this.index,
    @required this.value,
    @required this.label,
    @required this.icon,
    @required this.getItems,
    @required this.validatorFunction,
    @required this.setOnChangedValueForm,
    @required this.setOnSavedValueForm,
    this.theme = true
  });

  @override
  _LocationDropDownInputState createState() => _LocationDropDownInputState();
}

class _LocationDropDownInputState extends State<LocationDropDownInput> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 20.0),
      child: DropdownButtonFormField(
        value: widget.value,
        decoration: InputDecoration(
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: widget.theme ? Colors.white : CustomColors.mainGreen)
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: widget.theme ? CustomColors.secondaryGreen : CustomColors.mainDark)
          ),
          errorBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: widget.theme ? CustomColors.secondaryGreen : CustomColors.mainDark)
          ),
          focusedErrorBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: widget.theme ? CustomColors.secondaryGreen : CustomColors.mainDark)
          ),
          labelText: widget.label,
          labelStyle: TextStyle(color: widget.theme ? Colors.white : CustomColors.mainDark),
          errorStyle: TextStyle(color: widget.theme ? CustomColors.secondaryGreen : CustomColors.mainDark),
          icon: Icon(
            widget.icon,
            color: widget.theme ? Colors.white : CustomColors.mainDark
          ),
        ),
        dropdownColor: widget.theme ? CustomColors.mainDark : CustomColors.secondaryGreen,
        iconEnabledColor: widget.theme ? Colors.white : CustomColors.mainDark,
        iconDisabledColor: CustomColors.mainDark,
        style: TextStyle(color: widget.theme ? Colors.white : CustomColors.mainDark),
        items: widget.getItems,
        validator: (value) => widget.validatorFunction(widget.index, value),
        onChanged: (value) => widget.setOnChangedValueForm(widget.index, value),
        onSaved: (value) => widget.setOnSavedValueForm(widget.index, value)
      )
    );
  }

}
