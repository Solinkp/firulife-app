import 'package:flutter/material.dart';

import '../settings/custom_colors.dart';

class ColoredSafeArea extends StatelessWidget {
  final Widget child;
  final Color color;

  const ColoredSafeArea({
    @required this.child,
    this.color
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      color: color ?? CustomColors.mainGreen,
      child: SafeArea(
        left: true,
        right: true,
        child: child
      )
    );
  }

}
