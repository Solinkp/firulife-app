import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import '../routing/fade_route.dart';
import '../services/database_operation.dart';
import '../models/lost_and_found_post.dart';
import '../settings/custom_colors.dart';
import '../settings/placeholder_images.dart';
import '../screens/lost_found/lf_post_details_screen.dart';
import './custom_text_button.dart';
import './custom_snackbar.dart';

class MyPost extends StatelessWidget {
  final LostAndFoundPost post;
  final String uid;
  final BaseDatabaseOperation _db = DatabaseOperation();

  MyPost({@required this.post, @required this.uid});

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      InkWell(
        onTap: () => _openMyPostOptions(context),
        child: ListTile(
          leading: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              border: Border.all(color: CustomColors.mainGreen, width: 2)
            ),
            child: ClipRRect(
              borderRadius: const BorderRadius.all(Radius.circular(10)),
              child: FadeInImage(
                fit: BoxFit.fitWidth,
                placeholder: const AssetImage(PlaceholderImages.placeholderFiru),
                image: NetworkImage(post.pictures[0]),
                imageErrorBuilder: (BuildContext context, Object exception, StackTrace stackTrace) {
                  return Image.asset(PlaceholderImages.placeholderFiru);
                }
              )
            )
          ),
          title: Text('${post.name} - ${post.zoneLost}', style: Theme.of(context).textTheme.bodyText1),
          subtitle: Text('Fecha extravío: ${post.dayLost}\nPublicado: ${post.datePosted}', style: TextStyle(fontSize: 16))
        )
      ),
      Divider()
    ]);
  }

  void _openMyPostOptions(BuildContext context) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.mainDark,
          contentPadding: EdgeInsets.only(bottom: 15),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Stack(children: [
                Container(
                  alignment: Alignment.topRight,
                  child: IconButton(
                    color: CustomColors.secondaryGreen,
                    iconSize: 25,
                    icon: Icon(MdiIcons.closeCircleOutline),
                    onPressed: () {
                      Navigator.of(context).pop();
                    }
                  )
                ),
                Container(
                  padding: EdgeInsets.only(top: 25.0),
                  alignment: Alignment.bottomCenter,
                  child: Text('¿Qué deseas hacer?', style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.bold))
                )
              ]),
              Divider(),
              Row(children: [
                Expanded(
                  child: InkWell(
                    onTap: () {
                      Navigator.of(context).pop();
                      _showDeletePostAlert(context);
                    },
                    child: Column(children: [
                      Icon(
                        MdiIcons.trashCan,
                        size: 36,
                        color: CustomColors.secondaryGreen
                      ),
                      Text('Eliminar', textAlign: TextAlign.center, style: TextStyle(fontSize: 15, color: Colors.white))
                    ])
                  )
                ),
                Expanded(
                  child: InkWell(
                    onTap: () {
                      Navigator.of(context).pop();
                      Navigator.of(context).push(FadeRoute(page: LostAndFoundPostDetailsScreen(), arguments: post));
                    },
                    child: Column(children: [
                      Icon(
                        Icons.description,
                        size: 36,
                        color: CustomColors.secondaryGreen
                      ),
                      Text('Ver detalle', textAlign: TextAlign.center, style: TextStyle(fontSize: 15, color: Colors.white))
                    ])
                  )
                )
              ])
            ]
          )
        );
      }
    );
  }

  void _showDeletePostAlert(BuildContext context) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.mainDark,
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: Colors.white),
              children: <TextSpan>[
                TextSpan(text: '¡Advertencia!\n\n', style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: '¿Estás seguro(a) que deseas eliminar la publicación?')
              ]
            )
          ),
          actions: [
            CustomTextButton(
              text: 'Cancelar',
              pressCallback: () => Navigator.of(context).pop()
            ),
            CustomTextButton(
              text: 'Eliminar',
              pressCallback: () {
                Navigator.of(context).pop();
                _submitDeleteCall(context);
              }
            )
          ]
        );
      }
    );
  }

  void _submitDeleteCall(BuildContext context) async {
    String pictureOne = post.pictures[0];
    String pictureTwo = post.pictures.asMap()[1];
    _showSnack(context, 'Eliminando publicación.');
    _db.deleteLostAndFoundPost(uid, post.id, pictureOne, pictureTwo);
  }

  void _showSnack(BuildContext context, String text) {
    final messenger = ScaffoldMessenger.of(context);
    Widget snackBar = CustomSnackbar(
      message: text,
      hideSnack: () => messenger.hideCurrentSnackBar()
    ).build(context);
    messenger.showSnackBar(snackBar);
  }

}
