import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import '../settings/custom_colors.dart';
import './custom_snackbar.dart';

class InfoTile extends StatelessWidget {
  final IconData icon;
  final String title;
  final String value;
  final int option;
  final Color personalizedColor;

  InfoTile({
    @required this.icon,
    @required this.title,
    @required this.value,
    @required this.option,
    @required this.personalizedColor
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      child: InkWell(
        onTap: () => openLink(option, context),
        child: ListTile(
          leading: Icon(icon, color: personalizedColor ?? CustomColors.mainGreen),
          title: Text(title, style: TextStyle(fontSize: 17, color: CustomColors.mainDark))
        )
      )
    );
  }

  void openLink(to, context) async {
    String url = '';
    switch(to) {
      case 0:
        if(value != null && value.isNotEmpty) {
          url = "https://www.google.com/maps/search/?api=1&query=$value";
        }
        break;
      case 1:
        url = 'tel:$value';
        break;
    }
    if (url.isNotEmpty) {
      if (await canLaunch(url)) {
        await launch(url);
      }
    } else {
      final messenger = ScaffoldMessenger.of(context);
      Widget snackBar = CustomSnackbar(
        message: 'Lo sentimos, no hay coordenadas exactas por el momento.',
        hideSnack: () => messenger.hideCurrentSnackBar()
      ).build(context);
      messenger.showSnackBar(snackBar);
    }
  }

}
