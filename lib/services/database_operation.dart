import 'dart:async';
import 'dart:io';
import 'package:path/path.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cloud_functions/cloud_functions.dart';
import 'package:firebase_storage/firebase_storage.dart';

import '../models/pet.dart';
import '../models/product.dart';
import '../settings/locations.dart';

abstract class BaseDatabaseOperation {
  Stream<List<Pet>> getShelterPets(String shelterId);

  Stream<List<Product>> getBusinessProducts(String businessId);

  Future<dynamic> createLostAndFoundPost({
    String uid,
    String countryId,
    String dept,
    String pictureOne,
    String pictureTwo,
    String name,
    String description,
    String dayLost,
    String datePosted,
    String timeLost,
    String zoneLost,
    String phone,
    bool whatsapp,
    bool lost
  });

  Future<String> uploadPostPicture(String uid, File image);

  Future<String> deleteLostAndFoundPost(String uid, String id, String imageOne, String imageTwo);
}

class DatabaseOperation implements BaseDatabaseOperation {
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  final HttpsCallable _createLostAndFoundPost = FirebaseFunctions.instance.httpsCallable('createLostAndFoundPost');
  final HttpsCallable _deleteLostAndFoundPost = FirebaseFunctions.instance.httpsCallable('deleteLostAndFoundPost');

  Stream<List<Pet>> getShelterPets(String shelterId) {
    return _firestore.collection('pet').where('businessId', isEqualTo: shelterId).orderBy('name', descending: false).snapshots().map((snapshot) {
      return snapshot.docs.map((document) => Pet.buildPet(document.id, document)).toList();
    });
  }

  Stream<List<Product>> getBusinessProducts(String businessId) {
    return _firestore.collection('product').where('businessId', isEqualTo: businessId).orderBy('name', descending: false).snapshots().map((snapshot) {
      return snapshot.docs.map((document) => Product.buildProduct(document.id, document)).toList();
    });
  }

  Future<dynamic> createLostAndFoundPost({
    String uid,
    String countryId,
    String dept,
    String pictureOne,
    String pictureTwo,
    String name,
    String description,
    String dayLost,
    String datePosted,
    String timeLost,
    String zoneLost,
    String phone,
    bool whatsapp,
    bool lost
  }) async {
    String phoneCode = countryId == Locations.NICKEY ?  Locations.NICPHONECODE : Locations.SLVPHONECODE;
    var result = await _createLostAndFoundPost.call(<String, dynamic>{
      'uid': uid,
      'datePosted': datePosted,
      'countryId': countryId,
      'dept': dept,
      'pictureOne': pictureOne,
      'pictureTwo': pictureTwo,
      'name': name,
      'description': description,
      'dayLost': dayLost,
      'timeLost': timeLost,
      'zoneLost': zoneLost,
      'phone': '$phoneCode $phone',
      'whatsapp': whatsapp,
      'lost': lost
    });
    return result.data['uid'].toString().trim();
  }

  Future<String> uploadPostPicture(String uid, File image) async {
    String fileUrl;
    String fileName = basename(image.path);
    String path = 'lostfound/${uid.toString()}/$fileName';
    Reference storageReference = FirebaseStorage.instance.ref().child(path);
    await storageReference.putFile(image);
    await storageReference.getDownloadURL().then((url) {
      fileUrl = url;
    });
    return fileUrl;
  }

  Future<String> deleteLostAndFoundPost(String uid, String id, String urlOne, String urlTwo) async {
    String imageOne = urlOne.split('$uid%2F')[1].split('?alt')[0];
    String imageTwo = urlTwo != null ? urlTwo.split('$uid%2F')[1].split('?alt')[0] : '';

    var result = await _deleteLostAndFoundPost.call(<String, dynamic>{
      'uid': uid,
      'id': id,
      'imageOne': imageOne,
      "imageTwo": imageTwo
    });
    return result.data['pid'].toString().trim();
  }

}
