import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_functions/cloud_functions.dart';

abstract class BaseAuth {
  Future<dynamic> signIn(String email, String password);

  Future<dynamic> getUserClaims(User user);

  Future<dynamic> signUp(String email, String password, String displayName, String country, String dept);

  Future<void> signOut();

  Future<void> sendPasswordResetEmail(String email);

  Future<String> editUser(User user, String displayName, String country, String dept);
}

class Auth implements BaseAuth {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  final HttpsCallable _createFirulifeAppUser = FirebaseFunctions.instance.httpsCallable('createFirulifeAppUser');
  final HttpsCallable _editFirulifeAppUser = FirebaseFunctions.instance.httpsCallable('editFirulifeAppUser');

  Future<dynamic> signIn(String email, String password) async {
    int userType;
    String userCountry;
    String userDept;
    String message;
    UserCredential credential = await _firebaseAuth.signInWithEmailAndPassword(
      email: email, password: password
    );
    User user = credential.user;
    if(user != null) {
      await getUserClaims(user).then((claims) {
        userType = claims['type'];
        userCountry = claims['country'];
        userDept = claims['department'];
      });
      if(!user.emailVerified) {
        await user.sendEmailVerification();
        signOut();
        message = 'Favor verifique su correo para poder iniciar sesión en la aplicación.';
      } else if(userType != 2) {
        signOut();
        message = 'Usuario no destinado para app móvil.';
      }
    }
    return {'message': message, 'country': userCountry, 'department': userDept};
  }

  Future<dynamic> getUserClaims(User user) async {
    int userType;
    String userCountry;
    String userDept;
    await user.getIdTokenResult(true).then((idTokenResult) => {
      userType = idTokenResult.claims['type'],
      userCountry = idTokenResult.claims['country'],
      userDept = idTokenResult.claims['dept']
    });
    return {'type': userType, 'country': userCountry, 'department': userDept};
  }

  Future<dynamic> signUp(String email, String password, String displayName, String country, String dept) async {
    var result = await _createFirulifeAppUser.call(<String, dynamic>{
      'email': email,
      'password': password,
      'displayName': displayName,
      'country': country,
      'dept': dept
    });
    if(result.data['errorInfo'] != null) {
      return result.data;
    }
    UserCredential credential = await _firebaseAuth.signInWithEmailAndPassword(
      email: email, password: password
    );
    User user = credential.user;
    await user.sendEmailVerification();
    signOut();
    return result.data;
  }

  Future<void> signOut() async {
    return _firebaseAuth.signOut();
  }

  Future<void> sendPasswordResetEmail(String email) async {
    return _firebaseAuth.sendPasswordResetEmail(email: email);
  }

  Future<String> editUser(User user, String displayName, String country, String dept) async {
    var result = await _editFirulifeAppUser.call(<String, dynamic>{
      'id': user.uid,
      'email': user.email,
      'displayName': displayName,
      'country': country,
      'dept': dept
    });
    return result.data['uid'].toString().trim();
  }

}
