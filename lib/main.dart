import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';

import './settings/custom_colors.dart';
import './widgets/root_widget.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        StreamProvider<User>.value(
          value: FirebaseAuth.instance.authStateChanges(),
          initialData: null
        )
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Firulife',
        theme: ThemeData(
          appBarTheme: AppBarTheme(systemOverlayStyle: SystemUiOverlayStyle.dark, color: CustomColors.mainGreen),
          accentColor: CustomColors.mainGreen,
          textTheme: ThemeData.light().textTheme.copyWith(
            headline6: TextStyle(fontSize: 24, color: Colors.white, fontFamily: 'Courgette'),
            headline5: TextStyle(fontSize: 22, color: Colors.white, fontFamily: 'Courgette'),
            headline4: TextStyle(fontSize: 20, color: Colors.white, fontFamily: 'Courgette'),
            subtitle2: TextStyle(fontSize: 20, color: CustomColors.mainDark, fontFamily: 'Courgette'),
            bodyText2: TextStyle(fontSize: 18, color: Colors.white),
            bodyText1: TextStyle(fontSize: 16, color: CustomColors.mainDark)
          ),
          fontFamily: 'Quicksand',
          visualDensity: VisualDensity.adaptivePlatformDensity
        ),
        builder: (BuildContext context, Widget child) {
          final MediaQueryData data = MediaQuery.of(context);
          return MediaQuery(
            data: data.copyWith(
              textScaleFactor: data.textScaleFactor.clamp(1.0, 1.3)
            ),
            child: child
          );
        },
        home: RootWidget()
      )
    );
  }

}
