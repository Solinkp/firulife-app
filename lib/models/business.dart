import 'package:flutter/material.dart';

class Business {
  final String id;
  final int type; // 0 = shelter | 1 = vet | 2 = store | 3 = walkers | 4 = indep. vet
  final String name;
  final List<Map<String, String>> addresses;
  final String pictureUrl;
  final List<String> phone;
  final List<String> services;
  final List<Map<String, String>> openingHours;
  final Map<String, List<Map<String, String>>> donationAccounts;
  final List<Map<String, String>> contact;
  final String adoptionLink;
  final Color businessColor;
  final String countryId;

  Business({
    @required this.id,
    @required this.type,
    @required this.pictureUrl,
    @required this.name,
    @required this.addresses,
    @required this.phone,
    @required this.openingHours,
    @required this.services,
    @required this.donationAccounts,
    @required this.contact,
    @required this.adoptionLink,
    @required this.businessColor,
    @required this.countryId
  });

  static Business buildBusiness(id, data) {
    return Business(
      id: id,
      type: data['type'],
      pictureUrl: data['pictureUrl'],
      name: data['name'],
      addresses: data['addresses'] != null ? data['addresses'].cast<Map<String, String>>() : null,
      phone: data['phone'] != null ? data['phone'].cast<String>() : null,
      openingHours: data['openingHours'] != null ? data['openingHours'].cast<Map<String, String>>() : null,
      services: data['services'] != null ? data['services'].cast<String>() : null,
      donationAccounts: data['donationAccounts'] != null ? data['donationAccounts'].cast<String, List<Map<String, String>>>() : null,
      contact: data['contact'] != null ? data['contact'].cast<Map<String, String>>() : null,
      adoptionLink: data['adoptionLink'],
      businessColor: data['businessColor'] != null ? Color(int.parse(data['businessColor'])) : null,
      countryId: data['countryId']
    );
  }

}
