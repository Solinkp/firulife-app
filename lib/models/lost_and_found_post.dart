import 'package:flutter/foundation.dart';

import '../settings/date_formatter.dart';

class LostAndFoundPost {
  final String id;
  final String uid;
  final String datePosted;
  final String country;
  final String dept;
  final List<String> pictures;
  final String name;
  final String description;
  final String dayLost;
  final String timeLost;
  final String zoneLost;
  final String phone;
  final bool whatsapp;
  final bool lost;

  LostAndFoundPost({
    @required this.id,
    @required this.uid,
    @required this.datePosted,
    @required this.country,
    @required this.dept,
    @required this.pictures,
    @required this.name,
    @required this.description,
    @required this.dayLost,
    @required this.timeLost,
    @required this.zoneLost,
    @required this.phone,
    @required this.whatsapp,
    @required this.lost
  });

  static LostAndFoundPost buildPost(id, data) {
    return LostAndFoundPost(
      id: id,
      uid: data['uid'],
      datePosted: DateFormatter.getFormattedDate(DateTime.parse(data['datePosted'])),
      country: data['countryId'],
      dept: data['dept'],
      pictures: data['pictures'] != null ? data['pictures'].cast<String>() : null,
      name: data['name'],
      description: data['description'],
      dayLost: DateFormatter.getFormattedDate(DateTime.parse(data['dayLost'])),
      timeLost: data['timeLost'],
      zoneLost: data['zoneLost'],
      phone: data['phone'],
      whatsapp: data['whatsapp'],
      lost: data['lost']
    );
  }

}
