import 'package:flutter/foundation.dart';

class Pet {
  final String id;
  final String businessId;
  final String name;
  final bool sex;
  final DateTime age;
  final String pictureUrl;
  final String description;

  Pet({
    @required this.id,
    @required this.businessId,
    @required this.name,
    @required this.sex,
    @required this.age,
    @required this.pictureUrl,
    @required this.description
  });

  static Pet buildPet(id, data) {
    return Pet(
      id: id,
      businessId: data['businessId'],
      name: data['name'],
      sex: data['sex'],
      age: DateTime.parse(data['age']),
      pictureUrl: data['pictureUrl'],
      description: data['description']
    );
  }

}
