import 'package:flutter/material.dart';

import '../widgets/spinner_loader.dart';
import '../widgets/colored_safe_area.dart';
import '../settings/custom_colors.dart';

class LoadingScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ColoredSafeArea(
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text('Cargando Firulife...', style: Theme.of(context).textTheme.headline6),
          elevation: 0
        ),
        body: Container(
          color: CustomColors.mainGreen,
          alignment: Alignment.center,
          child: SpinnerLoader(color: Colors.white)
        )
      )
    );
  }

}
