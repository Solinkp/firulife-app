import 'package:flutter/material.dart';

import '../../routing/fade_route.dart';
import '../../settings/custom_colors.dart';
import '../../settings/pop_up_menu_options.dart';
import '../../widgets/colored_safe_area.dart';
import '../auth/login_screen.dart';
import '../info_screen.dart';
import './home.dart';

class NotLoggedHomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ColoredSafeArea(
      child: Scaffold(
        backgroundColor: CustomColors.mainGreen,
        appBar: AppBar(
          centerTitle: false,
          title: Text('Inicio', style: Theme.of(context).textTheme.headline6),
          elevation: 0,
          actions: [
            PopupMenuButton(
              color: CustomColors.mainDark,
              onSelected: (choice) => _choiceAction(context, choice),
              itemBuilder: (BuildContext context) {
                return PopUpMenuOptions.options.map((String option) {
                  return PopupMenuItem(
                    value: option,
                    child: Text(option, style: TextStyle(color: Colors.white))
                  );
                }).toList();
              }
            )
          ]
        ),
        body: Home()
      )
    );
  }

  void _choiceAction(BuildContext context, String choice) {
    if(choice == PopUpMenuOptions.login) {
      Navigator.of(context).push(FadeRoute(page: LoginScreen()));
    }
    if(choice == PopUpMenuOptions.about) {
      Navigator.of(context).push(FadeRoute(page: InfoScreen()));
    }
  }

}
