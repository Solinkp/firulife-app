import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:firebase_auth/firebase_auth.dart' show User;

import './not_logged_home_screen.dart';
import './logged_home_screen.dart';

class HomeScreen extends StatefulWidget {

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  @override
  Widget build(BuildContext context) {
    User _user = Provider.of<User>(context);

    if(_user == null) {
      return NotLoggedHomeScreen();
    } else {
      return LoggedHomeScreen();
    }
  }

}
