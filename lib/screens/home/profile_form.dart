import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:firebase_auth/firebase_auth.dart' show User;

import '../../services/authentication.dart';
import '../../settings/custom_colors.dart';
import '../../settings/locations.dart';
import '../../settings/custom_shared_preferences.dart';
import '../../widgets/colored_safe_area.dart';
import '../../widgets/spinner_loader.dart';
import '../../widgets/custom_text_form_field.dart';
import '../../widgets/location_drop_down_input.dart';
import '../../widgets/custom_button.dart';
import '../../widgets/custom_text_button.dart';
import '../../widgets/form_error_message.dart';

class ProfileForm extends StatefulWidget {

  @override
  _ProfileFormState createState() => _ProfileFormState();
}

class _ProfileFormState extends State<ProfileForm> {
  final BaseAuth _auth = Auth();
  final _formKey = GlobalKey<FormState>();
  String _displayName;
  String _country;
  String _dept;
  String _errorMessage;
  bool _isLoading;
  User _user;

  @override
  void initState() {
    _errorMessage = "";
    _isLoading = false;
    CustomSharedPreferences().getLocalCountry().then((value) {
      setState(() {
        _country = value;
      });
    });
    CustomSharedPreferences().getLocalDepartment().then((value) {
      setState(() {
        _dept = value;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _user = Provider.of<User>(context);
    if(_user != null) {
      _displayName = _user.displayName;
    }

    return ColoredSafeArea(
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text('Información de usuario', style: Theme.of(context).textTheme.headline6)
        ),
        body: Stack(
          children: <Widget>[
            _showForm(),
            _showLoader()
          ]
        )
      )
    );
  }

  Widget _showLoader() {
    if (_isLoading) {
      return Center(child: SpinnerLoader(color: Colors.white));
    }
    return Container(
      height: 0.0,
      width: 0.0,
    );
  }

  Widget _showForm() {
    return Container(
      alignment: Alignment.center,
      color: CustomColors.mainGreen,
      padding: EdgeInsets.only(left: 30.0, right: 30.0),
      child: Form(
        key: _formKey,
        child: ListView(
          children: <Widget>[
            Padding(padding: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0)),
            CustomTextFormField(
              inputType: TextInputType.name,
              obscured: false,
              hintText: 'Nombre',
              icon: Icons.person_outline,
              index: 0,
              last: false,
              setValueForm: _setValueForm,
              validatorFunction: _validatorFunction,
              initialValue: _displayName
            ),
            LocationDropDownInput(
              index: 0,
              value: _country,
              label: 'País',
              icon: Icons.flag_outlined,
              getItems: _getCountries(),
              validatorFunction: _dropDownvalidatorFunction,
              setOnChangedValueForm: _onChangedDropDownValue,
              setOnSavedValueForm: _onSavedDropDownValue
            ),
            LocationDropDownInput(
              index: 1,
              value: _dept,
              label: 'Departamento',
              icon: Icons.location_city_outlined,
              getItems: _getDepts(),
              validatorFunction: _dropDownvalidatorFunction,
              setOnChangedValueForm: _onChangedDropDownValue,
              setOnSavedValueForm: _onSavedDropDownValue
            ),
            CustomButton(
              topPadding: 35.0,
              botPadding: 20.0,
              height: 40.0,
              elevation: 5.0,
              buttonColor: CustomColors.mainGreen,
              text: 'Guardar',
              textSize: 20.0,
              textColor: Colors.white,
              pressCallback: _notifyLogoutOnUpdate,
            ),
            FormErrorMessage(
              errorMessage: _errorMessage,
              topPadding: 15,
              textColor: CustomColors.secondaryGreen
            )
          ]
        )
      )
    );
  }

  void _setValueForm(int index, String value) {
    setState(() {
      if(index == 0) {
        _displayName = value.trim();
      }
    });
  }

  String _validatorFunction(int index, dynamic value) {
    if(index == 0) {
      return value.toString().trim().isEmpty ? 'Nombre no puede estar vacío' : null;
    }
    return null;
  }

  List<DropdownMenuItem<String>> _getCountries() {
    List<DropdownMenuItem<String>> list = [];
    Locations.COUNTRIES.forEach((key, value) {
      list.add(DropdownMenuItem(child: Text(value), value: key));
    });
    return list;
  }

  List<DropdownMenuItem<String>> _getDepts() {
    List<DropdownMenuItem<String>> list = [];
    if(_country != null && _country == Locations.NICKEY) {
      Locations.NICDEPTS.forEach((dept) {
        list.add(DropdownMenuItem(child: Text(dept), value: dept));
      });
    }
    if(_country != null && _country == Locations.SLVKEY) {
      Locations.SLVDEPTS.forEach((dept) {
        list.add(DropdownMenuItem(child: Text(dept), value: dept));
      });
    }
    return list;
  }

  String _dropDownvalidatorFunction(int index, dynamic value) {
    if(index == 0) {
      return value == null ? 'Campo requerido' : null;
    } else if(index == 1) {
      return (value == null || value == 'Seleccione un Departamento') ? 'Campo requerido' : null;
    }
    return null;
  }

  void _onChangedDropDownValue(int index, dynamic value) {
    if(index == 0) {
      setState(() {
        _dept = Locations.NICDEPTS[0];
        _country = value;
      });
    } else if(index == 1) {
      setState(() {
        _dept = value;
      });
    }
  }

  void _onSavedDropDownValue(int index, dynamic value) {
    if(index == 0) {
      _country = value;
    } else if(index == 1) {
      _dept = value;
    }
  }

  void _notifyLogoutOnUpdate() {
    if(_isLoading) {
      return;
    }
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.mainDark,
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: Colors.white),
              children: <TextSpan>[
                TextSpan(text: '¡Advertencia!\n\n', style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: 'Si actualizas tu información deberás iniciar sesión nuevamente.')
              ]
            )
          ),
          actions: [
            CustomTextButton(
              text: 'Cancelar',
              pressCallback: () => Navigator.of(context).pop()
            ),
            CustomTextButton(
              text: 'Continuar',
              pressCallback: () {
                Navigator.of(context).pop();
                _validateAndSubmit();
              }
            )
          ]
        );
      }
    );
  }

  bool _validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void _validateAndSubmit() async {
    setState(() {
      _errorMessage = "";
      _isLoading = true;
    });
    if (_validateAndSave()) {
      _auth.editUser(_user, _displayName, _country, _dept).then((result) {
        print(result);
        setState(() {
          _isLoading = false;
        });
        if (result != null) {
          _notifyInfoUpdate();
        }
      }).catchError(_onEditError);
    } else {
      setState(() {
        _isLoading = false;
      });
    }
  }

  void _onEditError(e) {
    if(mounted) {
      setState(() {
        _isLoading = false;
        _errorMessage = e.message;
      });
    }
  }

  void _notifyInfoUpdate() {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.mainDark,
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: Colors.white),
              children: <TextSpan>[
                TextSpan(text: '¡Aviso!\n\n', style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: 'Tus datos han sido actualizados.')
              ]
            )
          ),
          actions: [
            CustomTextButton(
              text: 'Ok',
              pressCallback: () {
                Navigator.of(context).pop();
                Navigator.of(context).pop();
                _auth.signOut();
              }
            )
          ]
        );
      }
    );
  }

}
