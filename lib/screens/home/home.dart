import 'package:flutter/material.dart';
import 'package:responsive_grid/responsive_grid.dart';

import '../../settings/firu_assets.dart';
import '../../widgets/firu_segment.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double data = MediaQuery.of(context).devicePixelRatio;

    return Container(
      child: ResponsiveGridList(
        rowMainAxisAlignment: MainAxisAlignment.center,
        minSpacing: 20,
        desiredItemWidth: data <= 3.0 ? 150 : 130,
        squareCells: data <= 3.0 ? true : false,
        children: [
          FiruSegment(segmentId: 0, segmentName: 'Firu Hogares', segmentImage: FiruAssets.firuShelter),
          FiruSegment(segmentId: 1, segmentName: 'Firu Salud', segmentImage: FiruAssets.firuVet),
          FiruSegment(segmentId: 2, segmentName: 'Firu Tiendas', segmentImage: FiruAssets.firuStore),
          FiruSegment(segmentId: 3, segmentName: 'Firu Extravíos', segmentImage: FiruAssets.firuLostAndFound),
          // FiruSegment(segmentId: 4, segmentName: 'Firu Cuidadores', segmentImage: FiruAssets.firuWalk)
        ]
      )
    );
  }

}
