import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:firebase_auth/firebase_auth.dart' show User;

import '../../routing/fade_route.dart';
import '../../settings/custom_colors.dart';
import '../../services/authentication.dart';
import '../../widgets/custom_snackbar.dart';
import '../../widgets/custom_text_button.dart';
import '../info_screen.dart';
import './profile_form.dart';

class Profile extends StatefulWidget {

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> with SingleTickerProviderStateMixin {
  final BaseAuth _auth = Auth();
  AnimationController _controller;
  Animation<double> _iconTurns;

  @override
  void initState() {
    _controller = AnimationController(
      duration: Duration(milliseconds: 200),
      vsync: this
    );
    _iconTurns = _controller.drive(Tween<double>(begin: 0.0, end: 0.5).chain(CurveTween(curve: Curves.easeIn)));
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    User _user = Provider.of<User>(context);

    return SingleChildScrollView(
      child: Container(
        child: Column(children: [
          Center(child: Text('Hola ${_user.displayName}!', style: Theme.of(context).textTheme.headline4)),
          Divider(),
          ExpansionTile(
            trailing: RotationTransition(
              turns: _iconTurns,
              child: const Icon(Icons.expand_more, color: CustomColors.secondaryGreen)
            ),
            onExpansionChanged: (expanded) => expanded ? _controller.forward() : _controller.reverse(),
            backgroundColor: CustomColors.mainDark.withOpacity(0.3),
            childrenPadding: EdgeInsets.only(left: 30.0),
            leading: Icon(Icons.settings, color: CustomColors.secondaryGreen),
            title: Text('Ajustes', style: Theme.of(context).textTheme.bodyText2),
            children: [
              ListTile(
                leading: Icon(Icons.account_circle, color: CustomColors.secondaryGreen),
                title: Text('Editar datos personales', style: Theme.of(context).textTheme.bodyText2),
                onTap: () => Navigator.of(context).push(FadeRoute(page: ProfileForm()))
              ),
              ListTile(
                leading: Icon(Icons.lock_open, color: CustomColors.secondaryGreen),
                title: Text('Cambiar contraseña', style: Theme.of(context).textTheme.bodyText2),
                onTap: () => _showPasswordChangeAlert(context, _user.email)
              )
            ]
          ),
          Divider(),
          ListTile(
            leading: Icon(Icons.info_outline, color: CustomColors.secondaryGreen),
            title: Text('Acerca de Firulife', style: Theme.of(context).textTheme.bodyText2),
            onTap: () => Navigator.of(context).push(FadeRoute(page: InfoScreen()))
          ),
          Divider(),
          ListTile(
            leading: Icon(Icons.exit_to_app, color: CustomColors.secondaryGreen),
            title: Text('Cerrar sesión', style: Theme.of(context).textTheme.bodyText2),
            onTap: () => _showLogOutAlert(context)
          )
        ])
      )
    );
  }

  void _showPasswordChangeAlert(BuildContext context, String email) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.mainDark,
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: Colors.white),
              children: <TextSpan>[
                TextSpan(text: '¡Aviso!\n\n', style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: '¿Está seguro/a que desea cambiar su contraseña?')
              ]
            )
          ),
          actions: [
            CustomTextButton(
              text: 'Cancelar',
              pressCallback: () => Navigator.of(context).pop()
            ),
            CustomTextButton(
              text: 'Cambiar Contraseña',
              pressCallback: () {
                _auth.sendPasswordResetEmail(email);
                _showSnack(context, 'Te hemos enviado un correo para cambiar tu contraseña.');
                Navigator.of(context).pop();
              }
            )
          ]
        );
      }
    );
  }

  void _showSnack(BuildContext context, String text) {
    final messenger = ScaffoldMessenger.of(context);
    Widget snackBar = CustomSnackbar(
      message: text,
      hideSnack: () => messenger.hideCurrentSnackBar()
    ).build(context);
    messenger.showSnackBar(snackBar);
  }

  void _showLogOutAlert(BuildContext context) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.mainDark,
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: Colors.white),
              children: <TextSpan>[
                TextSpan(text: '¡Aviso!\n\n', style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: '¿Está seguro/a que desea cerrar sesión?')
              ]
            )
          ),
          actions: [
            CustomTextButton(
              text: 'Cancelar',
              pressCallback: () => Navigator.of(context).pop()
            ),
            CustomTextButton(
              text: 'Cerrar sesión',
              pressCallback: () {
                Navigator.of(context).pop();
                _auth.signOut();
              }
            )
          ]
        );
      }
    );
  }

}
