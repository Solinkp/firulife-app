import 'package:flutter/material.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';

import '../../settings/custom_colors.dart';
import '../../widgets/colored_safe_area.dart';
import './home.dart';
import './profile.dart';

class LoggedHomeScreen extends StatefulWidget {

  @override
  _LoggedHomeScreenState createState() => _LoggedHomeScreenState();
}

class _LoggedHomeScreenState extends State<LoggedHomeScreen> {
  final _bottomNavigationKey = GlobalKey<CurvedNavigationBarState>();
  int _page = 0;
  String _title = 'Inicio';

  @override
  Widget build(BuildContext context) {
    return ColoredSafeArea(
      child: GestureDetector(
        onHorizontalDragEnd: (val) => _onHorizontalDrag(val),
        child: Scaffold(
          backgroundColor: CustomColors.mainGreen,
          appBar: AppBar(
            centerTitle: false,
            title: Text(_title, style: Theme.of(context).textTheme.headline6),
            backgroundColor: Colors.transparent,
            elevation: 0
          ),
          bottomNavigationBar: CurvedNavigationBar(
            key: _bottomNavigationKey,
            index: 0,
            color: CustomColors.secondaryGreen,
            backgroundColor: CustomColors.mainGreen,
            buttonBackgroundColor: CustomColors.secondaryGreen,
            items: [
              Icon(Icons.home, color: CustomColors.mainDark),
              Icon(Icons.person, color: CustomColors.mainDark)
            ],
            height: 50.0,
            onTap: (index) => _swapPage(index)
          ),
          body: _getBody()
        )
      )
    );
  }

  Widget _getBody() {
    if(_page == 0) {
      return Home();
    } else {
      return Profile();
    }
  }

  void _swapPage(index) {
    setState(() {
      _page = index;
      if(index == 0) {
        _title = 'Inicio';
      } else {
        _title = 'Perfil';
      }
    });
  }

  void _onHorizontalDrag(DragEndDetails val) {
    double velX = val.velocity.pixelsPerSecond.dx.floorToDouble();
    double velY = val.velocity.pixelsPerSecond.dy.floorToDouble();

    if(velX.floor() < velY) {
      if(_page == 0) {
        _bottomNavigationKey.currentState.setPage(1);
      }
    }
    else if(velX.floor() > velY) {
      if(_page == 1) {
        _bottomNavigationKey.currentState.setPage(0);
      }
    }
  }

}
