import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:responsive_grid/responsive_grid.dart';

import '../../settings/custom_colors.dart';
import '../../models/lost_and_found_post.dart';
import '../../widgets/spinner_loader.dart';
import '../../widgets/lost_and_found_post_card.dart';

class LostAndFoundTimeline extends StatelessWidget {
  final String country;
  final String dept;

  LostAndFoundTimeline({
    @required this.country,
    @required this.dept
  });

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: FirebaseFirestore.instance.collection('lostfound').where('countryId', isEqualTo: country).where('dept', isEqualTo: dept).where('active', isEqualTo: true).limit(26).snapshots(),
      builder: (_, snapshot) {
        switch(snapshot.connectionState) {
          case ConnectionState.waiting:
            return SpinnerLoader(color: CustomColors.mainGreen);
            break;
          default:
            int postCount = snapshot.data.docs.length;
            return postCount == 0 ? Container(
              alignment: Alignment.center,
              child: Text(
                'No hay publicaciones de extravíos en el departamento seleccionado.',
                style: TextStyle(fontSize: 23, color: CustomColors.mainDark),
                textAlign: TextAlign.center
              )
            ) : ResponsiveGridList(
              rowMainAxisAlignment: MainAxisAlignment.center,
              minSpacing: 20,
              desiredItemWidth: 150,
              children: _getPosts(snapshot.data.docs)
            );
        }
      }
    );
  }

  List<Widget> _getPosts(dynamic data) {
    List<Widget> postsList = [];
    for(var i = 0; i < data.length; i++) {
      final String documentId = data[i].id;
      LostAndFoundPost post = LostAndFoundPost.buildPost(documentId, data[i].data());
      Widget postWidget = LostAndFoundPostCard(post: post);
      postsList.add(postWidget);
    }
    return postsList;
  }

}
