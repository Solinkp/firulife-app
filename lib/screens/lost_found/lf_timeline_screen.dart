import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:firebase_auth/firebase_auth.dart' show User;
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import '../../routing/fade_route.dart';
import '../../settings/custom_colors.dart';
import '../../settings/locations.dart';
import '../../settings/custom_shared_preferences.dart';
import '../../widgets/colored_safe_area.dart';
import '../../widgets/spinner_loader.dart';
import '../../widgets/custom_text_button.dart';
import '../auth/login_screen.dart';
import './lf_timeline.dart';
import './lf_my_posts.dart';
import './lf_post_form.dart';

class LostAndFoundTimelineScreen extends StatefulWidget {

  @override
  _LostAndFoundTimelineScreenState createState() => _LostAndFoundTimelineScreenState();
}

class _LostAndFoundTimelineScreenState extends State<LostAndFoundTimelineScreen> {
  final _bottomNavigationKey = GlobalKey<CurvedNavigationBarState>();
  int _page = 0;
  String _title = 'Firu Extravíos';
  String _country;
  String _dept;
  List<String> _deptList;
  bool _isLoading = true;
  User _user;

  @override
  void initState() {
    CustomSharedPreferences().getLocalCountry().then((value) {
      setState(() {
        _country = value;
        _deptList = _getDepts(_country);
      });
      _deptList.removeAt(0);
    });
    CustomSharedPreferences().getLocalDepartment().then((value) {
      setState(() {
        _dept = value;
        _isLoading = false;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _user = Provider.of<User>(context);

    return ColoredSafeArea(
      child: GestureDetector(
        onHorizontalDragEnd: (val) => _onHorizontalDrag(val),
        child: Scaffold(
          backgroundColor: CustomColors.secondaryGreen,
          appBar: AppBar(
            centerTitle: true,
            title: Text(_title, style: Theme.of(context).textTheme.headline6),
            actions: _page == 0 ? [
              IconButton(icon: Icon(Icons.filter_list), onPressed: () => _openFilterModal())
            ] : null
          ),
          bottomNavigationBar: CurvedNavigationBar(
            key: _bottomNavigationKey,
            index: 0,
            color: CustomColors.mainGreen,
            backgroundColor: CustomColors.secondaryGreen,
            buttonBackgroundColor: CustomColors.mainGreen,
            items: [
              Icon(MdiIcons.timelineTextOutline, color: CustomColors.secondaryGreen),
              Icon(Icons.pending_actions, color: CustomColors.secondaryGreen)
            ],
            height: 50.0,
            onTap: (index) => _swapPage(index)
          ),
          floatingActionButton: FloatingActionButton(
            backgroundColor: CustomColors.mainGreen,
            child: Icon(Icons.post_add, color: Colors.white, size: 30),
            onPressed: () => _user == null ? _openLoginMessage() : _openPostForm()
          ),
          body: _isLoading ? SpinnerLoader(color: CustomColors.mainGreen) : _getBody()
        )
      )
    );
  }

  Widget _getBody() {
    if(_page == 0) {
      return LostAndFoundTimeline(country: _country, dept: _dept);
    } else {
      return LostAndFoundMyPosts();
    }
  }

  void _swapPage(int index) {
    setState(() {
      _page = index;
      if(index == 0) {
        _title = 'Firu Extravíos';
      } else {
        _title = 'Mis Publicaciones';
      }
    });
  }

  void _onHorizontalDrag(DragEndDetails val) {
    double velX = val.velocity.pixelsPerSecond.dx.floorToDouble();
    double velY = val.velocity.pixelsPerSecond.dy.floorToDouble();

    if(velX.floor() < velY) {
      if(_page == 0) {
        _bottomNavigationKey.currentState.setPage(1);
      }
    }
    else if(velX.floor() > velY) {
      if(_page == 1) {
        _bottomNavigationKey.currentState.setPage(0);
      }
    }
  }

  List<String> _getDepts(String country) {
    List<String> list = [];
    if(country != null && country == Locations.NICKEY) {
      Locations.NICDEPTS.forEach((dept) {
        list.add(dept);
      });
    }
    if(country != null && country == Locations.SLVKEY) {
      Locations.SLVDEPTS.forEach((dept) {
        list.add(dept);
      });
    }
    return list;
  }

  void _openFilterModal() {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.mainDark,
          scrollable: true,
          contentPadding: EdgeInsets.all(5),
          titleTextStyle: TextStyle(fontSize: 17, color: Colors.white, fontWeight: FontWeight.bold),
          title: Center(child: Text('Filtro por departamento')),
          content: Column(children: _createRadioListDepts()),
          actions: [
            CustomTextButton(
              text: 'Cancelar',
              pressCallback: () => Navigator.of(context).pop()
            )
          ]
        );
      }
    );
  }

  List<Widget> _createRadioListDepts() {
    List<Widget> depts = [];
    for(String dept in _deptList) {
      depts.add(
        RadioListTile(
          value: dept,
          groupValue: _dept,
          title: Text(dept, style: TextStyle(color: Colors.white)),
          onChanged: (selectedDept) {
            _setSelectedDept(selectedDept);
            Navigator.of(context).pop();
          },
          selected: _dept == dept,
          activeColor: CustomColors.secondaryGreen
        )
      );
    }
    return depts;
  }

  void _setSelectedDept(String newDept) {
    setState(() {
      _dept = newDept;
    });
  }

  void _openLoginMessage() {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.mainDark,
          title: Center(child: Text('¡Aviso!')),
          titleTextStyle: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold),
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: Colors.white),
              children: <TextSpan>[
                TextSpan(text: 'Para publicar extravíos debes iniciar sesión.')
              ]
            )
          ),
          actions: [
            CustomTextButton(
              text: 'Cancelar',
              pressCallback: () => Navigator.of(context).pop()
            ),
            CustomTextButton(
              text: 'Iniciar sesión',
              pressCallback: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(FadeRoute(page: LoginScreen()));
              }
            )
          ]
        );
      }
    );
  }

  void _openPostForm() {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.mainDark,
          title: Center(child: Text('¿Qué deseas publicar?')),
          titleTextStyle: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Divider(),
              Container(
                height: 70,
                child: InkWell(
                  onTap: () {
                    Navigator.of(context).pop();
                    Navigator.of(context).push(FadeRoute(page: LostAndFoundPostForm(), arguments: true));
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Icon(
                        Icons.post_add,
                        size: 30,
                        color: CustomColors.secondaryGreen
                      ),
                      Text('Perdí a mi mascota', textAlign: TextAlign.center, style: TextStyle(fontSize: 16, color: Colors.white))
                    ]
                  )
                )
              ),
              Container(
                height: 70,
                child: InkWell(
                  onTap: () {
                    Navigator.of(context).pop();
                    Navigator.of(context).push(FadeRoute(page: LostAndFoundPostForm(), arguments: false));
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Icon(
                        Icons.post_add,
                        size: 30,
                        color: CustomColors.secondaryGreen
                      ),
                      Text('Encontré una mascota', textAlign: TextAlign.center, style: TextStyle(fontSize: 16, color: Colors.white))
                    ]
                  )
                )
              )
            ]
          ),
          actions: [
            CustomTextButton(
              text: 'Cancelar',
              pressCallback: () => Navigator.of(context).pop()
            )
          ]
        );
      }
    );
  }

}
