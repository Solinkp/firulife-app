import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:firebase_auth/firebase_auth.dart' show User;
import 'package:carousel_pro/carousel_pro.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:io';

import '../../models/lost_and_found_post.dart';
import '../../settings/custom_colors.dart';
import '../../settings/placeholder_images.dart';
import '../../widgets/colored_safe_area.dart';
import '../../widgets/custom_button.dart';

class LostAndFoundPostDetailsScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final LostAndFoundPost _post = ModalRoute.of(context).settings.arguments;
    User _user = Provider.of<User>(context);

    return ColoredSafeArea(
      child: Scaffold(
        backgroundColor: CustomColors.mainGreen,
        appBar: AppBar(
          centerTitle: true,
          title: Text(_post.lost ? '¡Información de Firu extraviado!' : '¡Información de Firu encontrado!'),
          elevation: 0
        ),
        body: Stack(children: [
          Positioned(
            top: 0.0,
            child: Container(
              padding: EdgeInsets.only(bottom: 5.0, left: 10.0, right: 10.0),
              height: 300.0,
              width: MediaQuery.of(context).size.width,
              alignment: Alignment.center,
              child: Carousel(
                autoplay: false,
                animationCurve: Curves.fastOutSlowIn,
                animationDuration: const Duration(milliseconds: 1000),
                dotSize: 4.0,
                dotIncreasedColor: CustomColors.secondaryGreen,
                dotColor: Colors.white,
                dotBgColor: Colors.transparent,
                dotPosition: DotPosition.bottomCenter,
                dotVerticalPadding: 5.0,
                showIndicator: _post.pictures.length > 1 ? true : false,
                indicatorBgPadding: 0.0,
                images: _post.pictures.map((picture) => Container(
                  margin: const EdgeInsets.symmetric(horizontal: 15),
                  alignment: Alignment.center,
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(22),
                      border: Border.all(color: CustomColors.secondaryGreen, width: 2)
                    ),
                    child: ClipRRect(
                      borderRadius: const BorderRadius.all(Radius.circular(20)),
                      child: FadeInImage(
                        fit: BoxFit.contain,
                        placeholder: const AssetImage(PlaceholderImages.placeholderFiru),
                        image: NetworkImage(picture),
                        imageErrorBuilder: (BuildContext context, Object exception, StackTrace stackTrace) {
                          return Image.asset(PlaceholderImages.placeholderFiru);
                        }
                      )
                    )
                  )
                )).toList()
              )
            )
          ),
          Positioned(
            top: 300.0,
            bottom: 0.0,
            width: MediaQuery.of(context).size.width,
            child: Container(
              decoration: BoxDecoration(
                color: CustomColors.secondaryGreen,
                borderRadius: const BorderRadius.only(topLeft: Radius.circular(20.0), topRight: Radius.circular(20.0)),
              ),
              child: ClipRRect(
                borderRadius: const BorderRadius.only(topLeft: Radius.circular(20.0), topRight: Radius.circular(20.0)),
                child: SingleChildScrollView(
                  child: Column(children: [
                    RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        style: const TextStyle(fontSize: 16, color: CustomColors.mainDark),
                        children: <TextSpan>[
                          TextSpan(text: '\nNombre:\n', style: const TextStyle(fontWeight: FontWeight.bold)),
                          TextSpan(text: _post.name.isNotEmpty ? '${_post.name}\n\n' : 'Desconocido\n\n'),
                          TextSpan(text: 'Características:\n', style: const TextStyle(fontWeight: FontWeight.bold)),
                          TextSpan(text: '${_post.description}\n\n'),
                          TextSpan(text: _post.lost ? 'Día y hora de extravío:\n' : 'Día y hora de encuentro:\n', style: const TextStyle(fontWeight: FontWeight.bold)),
                          TextSpan(text: '${_post.dayLost} | ${_post.timeLost}\n\n'),
                          TextSpan(text: _post.lost ? 'Zona de extravío:\n' : 'Zona de encuentro\n', style: const TextStyle(fontWeight: FontWeight.bold)),
                          TextSpan(text: '${_post.zoneLost}')
                        ]
                      )
                    ),
                    _user.uid != _post.uid ? CustomButton(
                      botPadding: 5.0,
                      topPadding: 15.0,
                      buttonColor: CustomColors.mainGreen,
                      textSize: 16.0,
                      text: _post.lost ? 'Contactar dueño(a)' : 'Contactar',
                      pressCallback: () => _goToContact(_post)
                    ) : Container(height: 0.0, width: 0.0)
                  ])
                )
              )
            )
          )
        ])
      )
    );
  }

  void _goToContact(LostAndFoundPost post) async {
    String link;
    if(Platform.isIOS) {
      link = post.whatsapp ? 'whatsapp://wa.me/${post.phone}' : 'tel:${post.phone}';
    } else {
      link = post.whatsapp ? 'whatsapp://send?phone=${post.phone}' : 'tel:${post.phone}';
    }
    if(await canLaunch(link)) {
      await launch(link);
    }
  }

}
