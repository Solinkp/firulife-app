import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:firebase_auth/firebase_auth.dart' show User;
import 'package:cloud_firestore/cloud_firestore.dart';

import '../../routing/fade_route.dart';
import '../../models/lost_and_found_post.dart';
import '../../settings/custom_colors.dart';
import '../../widgets/spinner_loader.dart';
import '../../widgets/custom_button.dart';
import '../../widgets/my_post.dart';
import '../auth/login_screen.dart';

class LostAndFoundMyPosts extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    User _user = Provider.of<User>(context);

    if(_user == null) {
      return _showLoginMessage(context);
    } else {
      return StreamBuilder(
        stream: FirebaseFirestore.instance.collection('lostfound').where('uid', isEqualTo: _user.uid).snapshots(),
        builder: (_, snapshot) {
          switch(snapshot.connectionState) {
            case ConnectionState.waiting:
              return SpinnerLoader(color: CustomColors.mainGreen);
              break;
            default:
              int postCount = snapshot.data.docs.length;
              return postCount == 0 ? Container(
                alignment: Alignment.center,
                child: Text(
                  'No has realizado publicaciones de extravíos.',
                  style: TextStyle(fontSize: 23, color: CustomColors.mainDark),
                  textAlign: TextAlign.center
                )
              ) : ListView.builder(
                itemCount: postCount,
                itemBuilder: (_, i) {
                  final String documentId = snapshot.data.docs[i].id;
                  LostAndFoundPost post = LostAndFoundPost.buildPost(documentId, snapshot.data.docs[i].data());
                  return MyPost(post: post, uid: _user.uid);
                }
              );
          }
        }
      );
    }
  }

  Widget _showLoginMessage(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            'Para ver tus publicaciones debes iniciar sesión.',
            style: TextStyle(fontSize: 23, color: CustomColors.mainDark),
            textAlign: TextAlign.center
          ),
          CustomButton(
            topPadding: 10.0,
            botPadding: 0.0,
            buttonColor: CustomColors.mainGreen,
            text: 'Iniciar sesión',
            textSize: 18.0,
            pressCallback: () => Navigator.of(context).push(FadeRoute(page: LoginScreen()))
          )
        ]
      )
    );
  }

}
