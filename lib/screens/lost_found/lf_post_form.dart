import 'package:flutter/material.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:firebase_auth/firebase_auth.dart' show User;

import '../../settings/custom_colors.dart';
import '../../settings/custom_shared_preferences.dart';
import '../../settings/locations.dart';
import '../../settings/date_formatter.dart';
import '../../services/database_operation.dart';
import '../../widgets/custom_text_button.dart';
import '../../widgets/colored_safe_area.dart';
import '../../widgets/spinner_loader.dart';
import '../../widgets/custom_text_form_field.dart';
import '../../widgets/location_drop_down_input.dart';
import '../../widgets/custom_button.dart';
import '../../widgets/form_error_message.dart';

class LostAndFoundPostForm extends StatefulWidget {
  final BaseDatabaseOperation db = DatabaseOperation();

  @override
  _LostAndFoundPostFormState createState() => _LostAndFoundPostFormState();
}

class _LostAndFoundPostFormState extends State<LostAndFoundPostForm> {
  final _formKey = GlobalKey<FormState>();
  double _imageSize = 140;
  List<File> _pictureList;
  String _pictureOnePath;
  String _pictureTwoPath = '';
  TextEditingController _nameCtrl;
  TextEditingController _descriptionCtrl;
  TextEditingController _dateCtrl;
  TextEditingController _timeCtrl;
  bool _lostPost;
  String _country;
  String _dept;
  TextEditingController _zoneLostCtrl;
  TextEditingController _phoneCtrl;
  bool _postWhatsapp = false;
  String _errorMessage;
  bool _isLoading = true;
  User _user;

  @override
  void initState() {
    _pictureList = [];
    _errorMessage = "";
    _nameCtrl = TextEditingController();
    _descriptionCtrl = TextEditingController();
    _dateCtrl = TextEditingController();
    _timeCtrl = TextEditingController();
    _zoneLostCtrl = TextEditingController();
    _phoneCtrl = TextEditingController();
    CustomSharedPreferences().getLocalCountry().then((value) {
      setState(() {
        _country = value;
      });
    });
    CustomSharedPreferences().getLocalDepartment().then((value) {
      setState(() {
        _dept = value;
        _isLoading = false;
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    _nameCtrl.dispose();
    _descriptionCtrl.dispose();
    _dateCtrl.dispose();
    _timeCtrl.dispose();
    _zoneLostCtrl.dispose();
    _phoneCtrl.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _user = Provider.of<User>(context);
    _lostPost = ModalRoute.of(context).settings.arguments;

    return ColoredSafeArea(
      child: Scaffold(
        backgroundColor: CustomColors.secondaryGreen,
        appBar: AppBar(
          centerTitle: true,
          title: Text('Nuevo Post', style: Theme.of(context).textTheme.headline6)
        ),
        body: Stack(
          children: <Widget>[
            _showForm(),
            _showLoader()
          ]
        )
      ),
    );
  }

  Widget _showLoader() {
    if (_isLoading) {
      return Center(child: SpinnerLoader(color: CustomColors.mainGreen));
    }
    return Container(
      height: 0.0,
      width: 0.0
    );
  }

  Widget _showForm() {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.only(left: 20.0, right: 20.0),
      child: Form(
        key: _formKey,
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            Padding(padding: const EdgeInsets.only(top: 20.0)),
            Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: _getImagePickers()
            ),
            // name | nombre mascota perdida - 1 line
            Padding(padding: const EdgeInsets.only(top: 20.0)),
            CustomTextFormField(
              theme: false,
              charLength: 20,
              inputType: TextInputType.text,
              obscured: false,
              hintText: _lostPost ? 'Nombre' : 'Nombre (Opcional)',
              icon: Icons.text_fields,
              index: 0,
              last: false,
              setValueForm: _setValueForm,
              validatorFunction: _validatorFunction
            ),
            // description - multi line
            CustomTextFormField(
              theme: false,
              lines: 3,
              charLength: 100,
              inputType: TextInputType.multiline,
              obscured: false,
              hintText: 'Descripción / Características',
              icon: Icons.text_fields,
              index: 1,
              last: false,
              setValueForm: _setValueForm,
              validatorFunction: _validatorFunction
            ),
            // dayLost - datePicker
            _showDatePickerInput(),
            // timeLost - timePicker
            _showTimePickerInput(),
            // dept - dropdown
            LocationDropDownInput(
              theme: false,
              index: 0,
              value: _dept,
              label: 'Departamento',
              icon: Icons.location_city,
              getItems: _getDepts(),
              validatorFunction: _dropDownvalidatorFunction,
              setOnChangedValueForm: _onChangedDropDownValue,
              setOnSavedValueForm: _onSavedDropDownValue
            ),
            // zoneLost - 1 line
            CustomTextFormField(
              theme: false,
              charLength: 50,
              inputType: TextInputType.text,
              obscured: false,
              hintText: _lostPost ? 'Zona de pérdida' : 'Zona encontrada',
              icon: Icons.text_fields,
              index: 2,
              last: false,
              setValueForm: _setValueForm,
              validatorFunction: _validatorFunction
            ),
            // phone - 1 line
            CustomTextFormField(
              theme: false,
              charLength: 8,
              inputType: TextInputType.phone,
              obscured: false,
              hintText: 'Teléfono de contacto',
              icon: Icons.phone,
              index: 3,
              last: false,
              setValueForm: _setValueForm,
              validatorFunction: _validatorFunction
            ),
            // whatsapp - checkbox for contact preference
            _showCheckbox(),
            CustomButton(
              topPadding: 35.0,
              botPadding: 20.0,
              height: 40.0,
              elevation: 5.0,
              buttonColor: CustomColors.mainGreen,
              text: 'Publicar',
              textSize: 20.0,
              textColor: Colors.white,
              pressCallback: _validateAndSubmit,
            ),
            FormErrorMessage(
              errorMessage: _errorMessage,
              topPadding: 10,
              textColor: CustomColors.mainDark
            )
          ]
        )
      )
    );
  }

  List<Widget> _getImagePickers() {
    List<Widget> list = [];
    File fileOne = _pictureList.asMap()[0];
    File fileTwo = _pictureList.asMap()[1];

    list.add(_showImagePickerInput(fileOne, 0));
    if(fileOne != null) {
      list.add(_showImagePickerInput(fileTwo, 1));
    }
    return list;
  }

  Widget _showImagePickerInput(File picture, int index) {
    return SizedBox(
      height: _imageSize,
      width: _imageSize,
      child: InkWell(
        onTap: () => _showPicturePicker(index),
        onLongPress: () => _removePicture(index),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: Border.all(color: CustomColors.mainDark)
          ),
          child: picture != null ? ClipRRect(
            borderRadius: const BorderRadius.all(Radius.circular(10)),
            child: Image.file(
              picture,
              fit: BoxFit.contain
            ),
          ) : Icon(
            Icons.camera_alt,
            color: Colors.grey
          )
        )
      )
    );
  }

  void _showPicturePicker(int index) {
    showModalBottomSheet(
      context: context,
      builder: (BuildContext context) {
        return ColoredSafeArea(
          child: Container(
            child: Wrap(
              children: <Widget>[
                ListTile(
                  leading: const Icon(Icons.photo_library, color: CustomColors.secondaryGreen),
                  title: const Text('Galería', style: TextStyle(color: Colors.white)),
                  onTap: () {
                    _getImageFromSource(ImageSource.gallery, index);
                    Navigator.of(context).pop();
                  }
                ),
                !_lostPost ?
                ListTile(
                  leading: const Icon(Icons.camera_alt, color: CustomColors.secondaryGreen),
                  title: const Text('Cámara', style: TextStyle(color: Colors.white)),
                  onTap: () {
                    _getImageFromSource(ImageSource.camera, index);
                    Navigator.of(context).pop();
                  }
                ) : Container()
              ]
            )
          )
        );
      }
    );
  }

  void _getImageFromSource(ImageSource source, int index) async {
    final ImagePicker picker = ImagePicker();
    PickedFile image = await picker.getImage(
      source: source,
      imageQuality: 50
    );

    if(image != null) {
      File file = _pictureList.asMap()[index];
      if(file != null) {
        _pictureList.removeAt(index);
      }
      setState(() {
        _pictureList.insert(index, File(image.path));
      });
    }
  }

  void _removePicture(int index) {
    File file = _pictureList.asMap()[index];
    if(file != null) {
      showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: CustomColors.mainDark,
            content: RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                style: TextStyle(fontSize: 16, color: Colors.white),
                children: <TextSpan>[
                  TextSpan(text: 'Eliminar Imagen\n\n', style: TextStyle(fontWeight: FontWeight.bold)),
                  TextSpan(text: '¿Desea eliminar la imagen seleccionada?')
                ]
              )
            ),
            actions: [
              CustomTextButton(
                text: 'Cancelar',
                pressCallback: () => Navigator.of(context).pop()
              ),
              CustomTextButton(
                text: 'Eliminar',
                pressCallback: () {
                  setState(() {
                    _pictureList.removeAt(index);
                  });
                  Navigator.of(context).pop();
                }
              )
            ]
          );
        }
      );
    }
  }

  Widget _showDatePickerInput() {
    return Padding(
      padding: const EdgeInsets.only(top: 15.0),
      child: TextFormField(
        controller: _dateCtrl,
        maxLines: 1,
        keyboardType: TextInputType.datetime,
        autofocus: false,
        cursorColor: CustomColors.mainDark,
        decoration: InputDecoration(
          focusedBorder: const UnderlineInputBorder(
            borderSide: const BorderSide(color: CustomColors.mainGreen)
          ),
          enabledBorder: const UnderlineInputBorder(
            borderSide: const BorderSide(color: CustomColors.mainGreen)
          ),
          errorBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: CustomColors.mainDark)
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(15)),
            borderSide: BorderSide(color: CustomColors.mainGreen)
          ),
          errorStyle: TextStyle(color: CustomColors.mainDark),
          hintText: _lostPost ? 'Día en que se perdió la mascota' : 'Día en que encontró la mascota',
          icon: Icon(
            Icons.calendar_today,
            color: CustomColors.mainDark
          )
        ),
        readOnly: true,
        onTap: () => _selectDate(),
        validator: (value) => value.isEmpty ? 'Día de pérdida no puede estar vacío' : null,
        onSaved: (value) => _dateCtrl.text = value
      )
    );
  }

  Future<void> _selectDate() async {
    final DateTime pickedDate = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime.now().subtract(Duration(days: 90)),
      lastDate: DateTime.now(),
      builder: (_, child) {
        return Theme(
          data: ThemeData.light().copyWith(
            colorScheme: ColorScheme.light(
              primary: CustomColors.mainGreen
            )
          ),
          child: child
        );
      }
    );
    if(pickedDate != null) {
      _dateCtrl.text = DateFormatter.convertDateTimeDisplay(pickedDate);
    }
  }

  Widget _showTimePickerInput() {
    return Padding(
      padding: const EdgeInsets.only(top: 20.0),
      child: TextFormField(
        controller: _timeCtrl,
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: false,
        cursorColor: CustomColors.mainDark,
        decoration: InputDecoration(
          focusedBorder: const UnderlineInputBorder(
            borderSide: const BorderSide(color: CustomColors.mainGreen)
          ),
          enabledBorder: const UnderlineInputBorder(
            borderSide: const BorderSide(color: CustomColors.mainGreen)
          ),
          errorBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: CustomColors.mainDark)
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(15)),
            borderSide: BorderSide(color: CustomColors.mainGreen)
          ),
          errorStyle: TextStyle(color: CustomColors.mainDark),
          hintText: _lostPost ? 'Hora en que se perdió la mascota' : 'Hora en que encontró la mascota',
          icon: Icon(
            Icons.access_time,
            color: CustomColors.mainDark
          )
        ),
        readOnly: true,
        onTap: () => _selectTime(context),
        validator: (value) => value.isEmpty ? 'Hora de pérdida no puede estar vacía' : null,
        onSaved: (value) => _timeCtrl.text = value
      )
    );
  }

  Future<void> _selectTime(BuildContext context) async {
    final TimeOfDay pickedTime = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
      builder: (_, child) {
        return Theme(
          data: ThemeData.light().copyWith(
            colorScheme: ColorScheme.light(
              primary: CustomColors.mainGreen
            )
          ),
          child: child
        );
      }
    );
    if(pickedTime != null) {
      _timeCtrl.text = pickedTime.format(context);
    }
  }

  List<DropdownMenuItem<String>> _getDepts() {
    List<DropdownMenuItem<String>> list = [];
    if(_country != null && _country == Locations.NICKEY) {
      Locations.NICDEPTS.forEach((dept) {
        list.add(DropdownMenuItem(child: Text(dept), value: dept));
      });
    }
    if(_country != null && _country == Locations.SLVKEY) {
      Locations.SLVDEPTS.forEach((dept) {
        list.add(DropdownMenuItem(child: Text(dept), value: dept));
      });
    }
    return list;
  }

  Widget _showCheckbox() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: CheckboxListTile(
        value: _postWhatsapp,
        title: Text('Prefiero ser contactado(a) por WhatsApp', style: TextStyle(color: CustomColors.mainDark)),
        onChanged: (value) {
          setState(() {
            _postWhatsapp = !_postWhatsapp;
          });
        }
      )
    );
  }

  void _setValueForm(int index, String value) {
    setState(() {
      if(index == 0) {
        _nameCtrl.text = value.trim();
      } else if(index == 1) {
        _descriptionCtrl.text = value.trim();
      } else if(index == 2) {
        _zoneLostCtrl.text = value.trim();
      } else if(index == 3) {
        _phoneCtrl.text = value.trim();
      }
    });
  }

  String _validatorFunction(int index, dynamic value) {
    if(index == 0) {
      return value.toString().trim().isEmpty && _lostPost ? 'Nombre no puede estar vacío' : null;
    } else if(index == 1) {
      return value.toString().trim().isEmpty ? 'Descripción no puede estar vacía' : null;
    } else if(index == 2) {
      return value.toString().trim().isEmpty ? 'Zona de pérdida no puede estar vacía' : null;
    } else if(index == 3) {
      return value.toString().trim().isEmpty ? 'Teléfono no puede estar vacío' : null;
    }
    return null;
  }

  String _dropDownvalidatorFunction(int index, dynamic value) {
    if(index == 0) {
      return (value == null || value == 'Seleccione un Departamento') ? 'Campo requerido' : null;
    }
    return null;
  }

  void _onChangedDropDownValue(int index, dynamic value) {
    if(index == 0) {
      setState(() {
        _dept = value;
      });
    }
  }

  void _onSavedDropDownValue(int index, dynamic value) {
    if(index == 0) {
      _dept = value;
    }
  }

  bool _validateAndSave() {
    final form = _formKey.currentState;
    File fileOne = _pictureList.asMap()[0];
    if(fileOne == null) {
      setState(() {
        _errorMessage = "Debes subir al menos una foto de la mascota";
      });
      return false;
    }
    if (form.validate() && fileOne != null) {
      form.save();
      return true;
    }
    return false;
  }

  void _validateAndSubmit() async {
    if(_isLoading) {
      return;
    }
    setState(() {
      _errorMessage = "";
      _isLoading = true;
    });
    if (_validateAndSave()) {
      widget.db.uploadPostPicture(_user.uid, _pictureList[0]).then((result) {
        _pictureOnePath = result;
        File fileTwo = _pictureList.asMap()[1];
        if(fileTwo != null) {
          widget.db.uploadPostPicture(_user.uid, _pictureList[1]).then((result) {
            _pictureTwoPath = result;
            _createPost();
          }).catchError((e) {print('fileTwo = $e');});
        } else {
          _createPost();
        }
      }).catchError((e) {print('fileOne  $e');});
    } else {
      setState(() {
        _isLoading = false;
      });
    }
  }

  void _createPost() async {
    String datePosted = DateFormatter.convertDateTimeDisplay(DateTime.now());
    widget.db.createLostAndFoundPost(
      uid: _user.uid,
      countryId: _country,
      dept: _dept,
      pictureOne: _pictureOnePath,
      pictureTwo: _pictureTwoPath,
      name: _nameCtrl.text.trim(),
      description: _descriptionCtrl.text.trim(),
      dayLost: _dateCtrl.text.trim(),
      datePosted: datePosted,
      timeLost: _timeCtrl.text.trim(),
      zoneLost: _zoneLostCtrl.text.trim(),
      phone: _phoneCtrl.text.trim(),
      whatsapp: _postWhatsapp,
      lost: _lostPost
    ).then((result) {
      if(result != null && result.length > 0) {
        setState(() {
          _isLoading = false;
        });
        _showSuccess(context);
      }
    }).catchError((e) {print('post = $e');});
  }

  void _showSuccess(BuildContext context) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.mainDark,
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: Colors.white),
              children: <TextSpan>[
                TextSpan(text: '¡Publicación realizada!\n\n', style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: 'Tu publicación se ha realizado exitosamente, puedes verla en el inicio de Firu Extravíos.')
              ]
            )
          ),
          actions: [
            CustomTextButton(
              text: 'Ok',
              pressCallback: () {
                Navigator.of(context).pop();
                Navigator.of(context).pop();
              }
            )
          ]
        );
      }
    );
  }

}
