import 'package:flutter/material.dart';

import '../routing/fade_route.dart';
import '../settings/custom_colors.dart';
import '../settings/locations.dart';
import '../settings/firu_assets.dart';
import '../settings/custom_shared_preferences.dart';
import '../widgets/colored_safe_area.dart';
import '../widgets/spinner_loader.dart';
import '../widgets/firu_logo.dart';
import '../widgets/form_error_message.dart';
import '../widgets/location_drop_down_input.dart';
import '../widgets/custom_button.dart';
import '../screens/home/home_screen.dart';

class CountryScreen extends StatefulWidget {

  @override
  _CountryScreenState createState() => _CountryScreenState();
}

class _CountryScreenState extends State<CountryScreen> {
  final _formKey = GlobalKey<FormState>();
  String _country;
  String _dept;
  String _errorMessage;
  bool _isLoading;

  @override
  void initState() {
    _errorMessage = "";
    _isLoading = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ColoredSafeArea(
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text('Selecciona tu ubicación', style: Theme.of(context).textTheme.headline6),
          elevation: 0,
        ),
        body: Stack(
          children: <Widget>[
            _showForm(),
            _showLoader()
          ]
        )
      )
    );
  }

  Widget _showLoader() {
    if (_isLoading) {
      return Center(child: SpinnerLoader(color: Colors.white));
    }
    return Container(
      height: 0.0,
      width: 0.0
    );
  }

  Widget _showForm() {
    return Container(
      alignment: Alignment.center,
      color: CustomColors.mainGreen,
      padding: EdgeInsets.only(left: 30.0, right: 30.0),
      child: Form(
        key: _formKey,
        child: Container(
          child: ListView(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            children: <Widget>[
              FiruLogo(color: Colors.transparent, radius: 70.0, imageAsset: FiruAssets.firuLogoWhite),
              LocationDropDownInput(
                index: 0,
                value: _country,
                label: 'País',
                icon: Icons.flag_outlined,
                getItems: _getCountries(),
                validatorFunction: _dropDownvalidatorFunction,
                setOnChangedValueForm: _onChangedDropDownValue,
                setOnSavedValueForm: _onSavedDropDownValue
              ),
              LocationDropDownInput(
                index: 1,
                value: _dept,
                label: 'Departamento',
                icon: Icons.location_city_outlined,
                getItems: _getDepts(),
                validatorFunction: _dropDownvalidatorFunction,
                setOnChangedValueForm: _onChangedDropDownValue,
                setOnSavedValueForm: _onSavedDropDownValue
              ),
              CustomButton(
                topPadding: 35.0,
                botPadding: 0.0,
                height: 40.0,
                elevation: 5.0,
                buttonColor: CustomColors.mainGreen,
                text: 'Continuar',
                textSize: 20.0,
                textColor: Colors.white,
                pressCallback: _validateAndSubmit,
              ),
              FormErrorMessage(
                errorMessage: _errorMessage,
                topPadding: 15,
                textColor: CustomColors.secondaryGreen
              )
            ]
          )
        )
      )
    );
  }

  List<DropdownMenuItem<String>> _getCountries() {
    List<DropdownMenuItem<String>> list = [];
    Locations.COUNTRIES.forEach((key, value) {
      list.add(DropdownMenuItem(child: Text(value), value: key));
    });
    return list;
  }

  List<DropdownMenuItem<String>> _getDepts() {
    List<DropdownMenuItem<String>> list = [];
    if(_country != null && _country == Locations.NICKEY) {
      Locations.NICDEPTS.forEach((dept) {
        list.add(DropdownMenuItem(child: Text(dept), value: dept));
      });
    }
    if(_country != null && _country == Locations.SLVKEY) {
      Locations.SLVDEPTS.forEach((dept) {
        list.add(DropdownMenuItem(child: Text(dept), value: dept));
      });
    }
    return list;
  }

  String _dropDownvalidatorFunction(int index, dynamic value) {
    if(index == 0) {
      return value == null ? 'Por favor selecciona un país' : null;
    } else if(index == 1) {
      return (value == null || value == 'Seleccione un Departamento') ? 'Por favor selecciona un departamento' : null;
    }
    return null;
  }

  void _onChangedDropDownValue(int index, dynamic value) {
    if(index == 0) {
      setState(() {
        _dept = Locations.NICDEPTS[0];
        _country = value;
      });
    } else if(index == 1) {
      setState(() {
        _dept = value;
      });
    }
  }

  void _onSavedDropDownValue(int index, dynamic value) {
    if(index == 0) {
      _country = value;
    } else if(index == 1) {
      _dept = value;
    }
  }

  bool _validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void _validateAndSubmit() async {
    if(_isLoading) {
      return;
    }
    setState(() {
      _errorMessage = "";
      _isLoading = true;
    });
    if (_validateAndSave()) {
      CustomSharedPreferences().setLocalCountry(_country).then((result) {
        CustomSharedPreferences().setLocalDepartment(_dept).then((result) {
          _goHome();
        });
      });
    } else {
      setState(() {
        _isLoading = false;
      });
    }
  }

  void _goHome() {
    Navigator.of(context).pushAndRemoveUntil(FadeRoute(page: HomeScreen()), (Route<dynamic> route) => false);
  }

}
