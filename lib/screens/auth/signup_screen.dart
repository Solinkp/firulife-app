import 'package:flutter/material.dart';

import '../../settings/custom_colors.dart';
import '../../settings/locations.dart';
import '../../settings/firu_assets.dart';
import '../../services/authentication.dart';
import '../../widgets/colored_safe_area.dart';
import '../../widgets/spinner_loader.dart';
import '../../widgets/firu_logo.dart';
import '../../widgets/custom_text_form_field.dart';
import '../../widgets/custom_button.dart';
import '../../widgets/custom_text_button.dart';
import '../../widgets/location_drop_down_input.dart';
import '../../widgets/form_error_message.dart';

class SignUpScreen extends StatefulWidget {

  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final BaseAuth _auth = Auth();
  final _formKey = GlobalKey<FormState>();
  String _displayName;
  String _country;
  String _dept;
  String _email;
  String _password;
  String _errorMessage;
  bool _isLoading;

  @override
  void initState() {
    _errorMessage = "";
    _isLoading = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ColoredSafeArea(
      child: Scaffold(
        appBar: AppBar(
          elevation: 0
        ),
        body: Stack(
          children: <Widget>[
            _showForm(),
            _showLoader()
          ]
        )
      )
    );
  }

  Widget _showLoader() {
    if (_isLoading) {
      return Center(child: SpinnerLoader(color: Colors.white));
    }
    return Container(
      height: 0.0,
      width: 0.0
    );
  }

  Widget _showForm() {
    return Container(
      alignment: Alignment.center,
      color: CustomColors.mainGreen,
      padding: EdgeInsets.only(left: 30.0, right: 30.0),
      child: Form(
        key: _formKey,
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            FiruLogo(color: Colors.transparent, radius: 70, imageAsset: FiruAssets.firuLogoWhite),
            Padding(padding: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0)),
            CustomTextFormField(
              inputType: TextInputType.name,
              obscured: false,
              hintText: 'Nombre',
              icon: Icons.person_outline,
              index: 0,
              last: false,
              setValueForm: _setValueForm,
              validatorFunction: _validatorFunction,
            ),
            LocationDropDownInput(
              index: 0,
              value: _country,
              label: 'País',
              icon: Icons.flag_outlined,
              getItems: _getCountries(),
              validatorFunction: _dropDownvalidatorFunction,
              setOnChangedValueForm: _onChangedDropDownValue,
              setOnSavedValueForm: _onSavedDropDownValue
            ),
            _country != null ? LocationDropDownInput(
              index: 1,
              value: _dept,
              label: 'Departamento',
              icon: Icons.location_city_outlined,
              getItems: _getDepts(),
              validatorFunction: _dropDownvalidatorFunction,
              setOnChangedValueForm: _onChangedDropDownValue,
              setOnSavedValueForm: _onSavedDropDownValue
            ) : Container(height: 0.0, width: 0.0),
            CustomTextFormField(
              inputType: TextInputType.emailAddress,
              obscured: false,
              hintText: 'Correo',
              icon: Icons.email_outlined,
              index: 1,
              last: false,
              setValueForm: _setValueForm,
              validatorFunction: _validatorFunction,
            ),
            CustomTextFormField(
              inputType: TextInputType.text,
              obscured: true,
              hintText: 'Contraseña',
              icon: Icons.lock_outline,
              index: 2,
              last: true,
              setValueForm: _setValueForm,
              validatorFunction: _validatorFunction,
            ),
            CustomButton(
              topPadding: 35.0,
              botPadding: 20.0,
              height: 40.0,
              elevation: 5.0,
              buttonColor: CustomColors.mainGreen,
              text: 'Registrar',
              textSize: 20.0,
              textColor: Colors.white,
              pressCallback: _validateAndSubmit,
            ),
            FormErrorMessage(
              errorMessage: _errorMessage,
              topPadding: 15,
              textColor: CustomColors.secondaryGreen
            )
          ]
        )
      )
    );
  }

  void _setValueForm(int index, String value) {
    setState(() {
      if(index == 0) {
        _displayName = value.trim();
      } else if(index == 1) {
        _email = value.trim();
      } else if(index == 2) {
        _password = value.trim();
      }
    });
  }

  String _validatorFunction(int index, dynamic value) {
    if(index == 0) {
      return value.toString().trim().isEmpty ? 'Nombre no puede estar vacío' : null;
    } else if(index == 1) {
      return value.toString().trim().isEmpty ? 'Correo no puede estar vacío' : null;
    } else if(index == 2) {
      if(value.toString().trim().isEmpty) {
        return 'Contraseña no puede estar vacía';
      } else if(value.toString().trim().length < 6) {
        return 'Contraseña demasiado corta.';
      } else {
        RegExp regex = RegExp(r'^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%!\-_?&])(?=\S+$).{6,}');
        if(!regex.hasMatch(value.toString().trim())) {
          return 'Su contraseña debe contener al menos\n1 mayúscula, 1 número y 1 caracter especial';
        }
      }
    }
    return null;
  }

  List<DropdownMenuItem<String>> _getCountries() {
    List<DropdownMenuItem<String>> list = [];
    Locations.COUNTRIES.forEach((key, value) {
      list.add(DropdownMenuItem(child: Text(value), value: key));
    });
    return list;
  }

  List<DropdownMenuItem<String>> _getDepts() {
    List<DropdownMenuItem<String>> list = [];
    if(_country != null && _country == Locations.NICKEY) {
      Locations.NICDEPTS.forEach((dept) {
        list.add(DropdownMenuItem(child: Text(dept), value: dept));
      });
    }
    if(_country != null && _country == Locations.SLVKEY) {
      Locations.SLVDEPTS.forEach((dept) {
        list.add(DropdownMenuItem(child: Text(dept), value: dept));
      });
    }
    return list;
  }

  String _dropDownvalidatorFunction(int index, dynamic value) {
    if(index == 0) {
      return value == null ? 'Campo requerido' : null;
    } else if(index == 1) {
      return (value == null || value == 'Seleccione un Departamento') ? 'Campo requerido' : null;
    }
    return null;
  }

  void _onChangedDropDownValue(int index, dynamic value) {
    if(index == 0) {
      setState(() {
        _dept = Locations.NICDEPTS[0];
        _country = value;
      });
    } else if(index == 1) {
      setState(() {
        _dept = value;
      });
    }
  }

  void _onSavedDropDownValue(int index, dynamic value) {
    if(index == 0) {
      _country = value;
    } else if(index == 1) {
      _dept = value;
    }
  }

  bool _validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void _validateAndSubmit() async {
    if(_isLoading) {
      return;
    }
    setState(() {
      _errorMessage = "";
      _isLoading = true;
    });
    if (_validateAndSave()) {
      _auth.signUp(_email, _password, _displayName, _country, _dept).then((result) {
        setState(() {
          _isLoading = false;
        });
        if(result['errorInfo'] != null) {
          String errorCode = result['errorInfo']['code'].toString().split('/')[1];
          _onSignUpError(errorCode);
        } else {
          _resetForm();
          _showVerifyAccountMessage();
        }
      });
    } else {
      setState(() {
        _isLoading = false;
      });
    }
  }

  void _resetForm() {
    setState(() {
      _formKey.currentState.reset();
      _errorMessage = "";
      _isLoading = false;
    });
  }

  void _showVerifyAccountMessage() {
    final String message = 'Se te ha enviado un mensaje con un enlace de verificación ' +
                           'al correo que has proporcionado.\nPor favor revisa tu correo y ' +
                           'verifica tu cuenta para poder iniciar sesión en la aplicación.\n\n' +
                           'Revisa la bandeja de spam si no ves el correo.\n¡Muchas gracias ' +
                           'por registrarte!';
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.mainDark,
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: Colors.white),
              children: <TextSpan>[
                TextSpan(text: '¡Aviso Importante!\n\n', style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: message)
              ]
            )
          ),
          actions: [
            CustomTextButton(
              text: 'Ok',
              pressCallback: () {
                Navigator.of(context).pop();
                Navigator.of(context).pop();
              }
            )
          ]
        );
      }
    );
  }

  void _onSignUpError(e) {
    if(mounted) {
      setState(() {
        _isLoading = false;
        switch(e) {
          case 'email-already-exists':
            _errorMessage = 'El correo ya se encuentra registrado.';
            break;
          default:
            _errorMessage = e;
        }
      });
    }
  }

}
