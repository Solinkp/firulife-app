import 'package:flutter/material.dart';

import '../../services/authentication.dart';
import '../../settings/custom_colors.dart';
import '../../settings/firu_assets.dart';
import '../../widgets/colored_safe_area.dart';
import '../../widgets/custom_snackbar.dart';
import '../../widgets/spinner_loader.dart';
import '../../widgets/firu_logo.dart';
import '../../widgets/custom_text_form_field.dart';
import '../../widgets/custom_button.dart';
import '../../widgets/form_error_message.dart';

class ResetPasswordScreen extends StatefulWidget {

  @override
  _ResetPasswordScreenState createState() => _ResetPasswordScreenState();
}

class _ResetPasswordScreenState extends State<ResetPasswordScreen> {
  final BaseAuth _auth = Auth();
  final _formKey = GlobalKey<FormState>();
  String _email;
  String _errorMessage;
  bool _isLoading;

  @override
  void initState() {
    _errorMessage = "";
    _isLoading = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ColoredSafeArea(
      child: Scaffold(
        appBar: AppBar(
          elevation: 0
        ),
        body: Stack(
          children: <Widget>[
            _showForm(),
            _showLoader()
          ]
        )
      )
    );
  }

  Widget _showLoader() {
    if (_isLoading) {
      return Center(child: SpinnerLoader(color: Colors.white));
    }
    return Container(
      height: 0.0,
      width: 0.0
    );
  }

  Widget _showForm() {
    return Container(
      alignment: Alignment.center,
      color: CustomColors.mainGreen,
      padding: EdgeInsets.only(left: 30.0, right: 30.0),
      child: Form(
        key: _formKey,
        child: Container(
          child: ListView(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            children: <Widget>[
              FiruLogo(color: Colors.transparent, radius: 70.0, imageAsset: FiruAssets.firuLogoWhite),
              Padding(padding: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0)),
              CustomTextFormField(
                inputType: TextInputType.emailAddress,
                obscured: false,
                hintText: 'Correo',
                icon: Icons.mail,
                index: 0,
                last: true,
                setValueForm: _setValueForm,
                validatorFunction: _validatorFunction,
              ),
              CustomButton(
                topPadding: 35.0,
                botPadding: 0.0,
                height: 40.0,
                elevation: 5.0,
                buttonColor: CustomColors.mainGreen,
                text: 'Recuperar Contraseña',
                textSize: 20.0,
                textColor: Colors.white,
                pressCallback: _validateAndSubmit,
              ),
              FormErrorMessage(
                errorMessage: _errorMessage,
                topPadding: 15,
                textColor: CustomColors.secondaryGreen
              )
            ]
          )
        )
      )
    );
  }

  void _setValueForm(int index, String value) {
    if(index == 0) {
      _email = value.trim();
    }
  }

  String _validatorFunction(int index, dynamic value) {
    if(index == 0) {
      return value.toString().trim().isEmpty ? 'Correo no puede estar vacío' : null;
    }
    return null;
  }

  bool _validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void _validateAndSubmit() async {
    if(_isLoading) {
      return;
    }
    setState(() {
      _errorMessage = "";
      _isLoading = true;
    });
    if (_validateAndSave()) {
      _auth.sendPasswordResetEmail(_email).then((_) {
        setState(() {
          _isLoading = false;
        });
        _formKey.currentState.reset();
        _showSnack('Te hemos enviado un correo para recuperar tu contraseña, revisa tu bandeja de entrada.');
      }).catchError(_onResetPasswordError);
    } else {
      setState(() {
        _isLoading = false;
      });
    }
  }

  void _showSnack(String text) {
    final messenger = ScaffoldMessenger.of(context);
    Widget snackBar = CustomSnackbar(
      message: text,
      hideSnack: () => messenger.hideCurrentSnackBar()
    ).build(context);
    messenger.showSnackBar(snackBar);
  }

  void _onResetPasswordError(e) {
    if(mounted) {
      setState(() {
        _isLoading = false;
        switch(e.code) {
          case 'invalid-email':
            _errorMessage = 'Correo mal escrito.';
            break;
          case 'user-not-found':
            _errorMessage = 'No se encontró ningún usuario con el correo ingresado.';
            break;
          case 'user-disabled':
            _errorMessage = 'Este usuario ha sido deshabilitado por un administrador.';
            break;
          default:
            _errorMessage = 'Ocurrió un problema, por favor intenta de nuevo más tarde.';
        }
      });
    }
  }

}
