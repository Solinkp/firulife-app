import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import '../../services/authentication.dart';
import '../../settings/custom_colors.dart';
import '../../settings/firu_assets.dart';
import '../../settings/custom_shared_preferences.dart';
import '../../routing/fade_route.dart';
import '../../widgets/colored_safe_area.dart';
import '../../widgets/spinner_loader.dart';
import '../../widgets/firu_logo.dart';
import '../../widgets/custom_text_form_field.dart';
import '../../widgets/custom_button.dart';
import '../../widgets/form_error_message.dart';
import './signup_screen.dart';
import './reset_password_screen.dart';

class LoginScreen extends StatefulWidget {

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final BaseAuth _auth = Auth();
  final _formKey = GlobalKey<FormState>();
  String _email;
  TextEditingController _emailController = TextEditingController();
  String _password;
  String _errorMessage;
  bool _isLoading;

  @override
  void initState() {
    _errorMessage = "";
    _isLoading = false;
    CustomSharedPreferences().getLocalEmail().then((value) {
      if(mounted){
        setState(() {
          _emailController.text = value;
        });
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ColoredSafeArea(
      child: Scaffold(
        appBar: AppBar(
          elevation: 0
        ),
        body: Stack(
          children: <Widget>[
            _showForm(),
            _showLoader()
          ]
        )
      )
    );
  }

  Widget _showLoader() {
    if (_isLoading) {
      return Center(child: SpinnerLoader(color: Colors.white));
    }
    return Container(
      height: 0.0,
      width: 0.0
    );
  }

  Widget _showForm() {
    return Container(
      alignment: Alignment.center,
      color: CustomColors.mainGreen,
      padding: EdgeInsets.only(left: 30.0, right: 30.0),
      child: Form(
        key: _formKey,
        child: Container(
          child: ListView(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            children: <Widget>[
              FiruLogo(color: Colors.transparent, radius: 70.0, imageAsset: FiruAssets.firuLogoWhite),
              Padding(padding: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0)),
              CustomTextFormField(
                inputType: TextInputType.emailAddress,
                obscured: false,
                hintText: 'Correo',
                icon: Icons.mail,
                index: 0,
                last: false,
                setValueForm: _setValueForm,
                validatorFunction: _validatorFunction,
                textController: _emailController
              ),
              CustomTextFormField(
                inputType: TextInputType.text,
                obscured: true,
                hintText: 'Contraseña',
                icon: Icons.lock,
                index: 1,
                last: true,
                setValueForm: _setValueForm,
                validatorFunction: _validatorFunction,
              ),
              CustomButton(
                topPadding: 35.0,
                botPadding: 0.0,
                height: 40.0,
                elevation: 5.0,
                buttonColor: CustomColors.mainGreen,
                text: 'Iniciar Sesión',
                textSize: 20.0,
                textColor: Colors.white,
                pressCallback: _validateAndSubmit,
              ),
              FormErrorMessage(
                errorMessage: _errorMessage,
                topPadding: 15,
                textColor: CustomColors.secondaryGreen
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: Center(
                  child: RichText(
                    text: TextSpan(
                      style: TextStyle(fontSize: 15, color: Colors.white, decoration: TextDecoration.underline),
                      text: 'Olvidé mi contraseña',
                      recognizer: TapGestureRecognizer()..onTap = _goToRecoverPassword
                    )
                  )
                )
              ),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Center(
                  child: RichText(
                    text: TextSpan(
                      style: TextStyle(fontSize: 17),
                      children: [
                        TextSpan(text: '¿Primera vez en Firulife? ', style: TextStyle(color: Colors.white)),
                        TextSpan(
                          text: 'Regístrate',
                          style: TextStyle(color: CustomColors.mainDark, fontWeight: FontWeight.bold),
                          recognizer: TapGestureRecognizer()..onTap = _goToSignUpScreen
                        )
                      ]
                    )
                  )
                )
              )
            ]
          )
        )
      )
    );
  }

  void _setValueForm(int index, String value) {
    if(index == 0) {
      _email = value.trim();
    } else {
      _password = value.trim();
    }
  }

  String _validatorFunction(int index, dynamic value) {
    if(index == 0) {
      return value.toString().trim().isEmpty ? 'Correo no puede estar vacío' : null;
    } else if(index == 1) {
      return value.toString().trim().isEmpty ? 'Contraseña no puede estar vacía' : null;
    }
    return null;
  }

  bool validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void _validateAndSubmit() async {
    if(_isLoading) {
      return;
    }
    setState(() {
      _errorMessage = "";
      _isLoading = true;
    });
    if (validateAndSave()) {
      await CustomSharedPreferences().setLocalEmail(_email);
      _auth.signIn(_email, _password).then((result) {
        if(result['country'] != null) {
          String country = result['country'];
          String dept = result['department'];
          CustomSharedPreferences().setLocalCountry(country).then((result) {
            CustomSharedPreferences().setLocalDepartment(dept).then((result) {
              if(mounted) {
                setState(() {
                  _isLoading = false;
                });
                Navigator.of(context).pop();
              }
            });
          });
        } else {
          if(mounted) {
            setState(() {
              _isLoading = false;
              _errorMessage = result['message'];
            });
          }
        }
      }).catchError(_onSignInError);
    } else {
      setState(() {
        _isLoading = false;
      });
    }
  }

  void _onSignInError(e) {
    if(mounted){
      setState(() {
        _isLoading = false;
        switch(e.code) {
          case 'invalid-email':
            _errorMessage = 'Correo mal escrito.';
            break;
          case 'wrong-password':
            _errorMessage = 'Contraseña equivocada.';
            break;
          case 'user-not-found':
            _errorMessage = 'No se encontró ningún usuario con el correo ingresado.';
            break;
          case 'too-many-requests':
            _errorMessage = 'Acceso bloqueado temporalmente debido a muchos intentos fallidos, por favor intente de nuevo más tarde.';
            break;
          case 'user-disabled':
            _errorMessage = 'Este usuario ha sido deshabilitado por un administrador.';
            break;
          default:
            _errorMessage = e.message;
        }
      });
    }
  }

  void _goToRecoverPassword() {
    Navigator.of(context).push(FadeRoute(page: ResetPasswordScreen()));
  }

  void _goToSignUpScreen() {
    Navigator.of(context).push(FadeRoute(page: SignUpScreen()));
  }

}
