import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';

import '../settings/custom_colors.dart';
import '../settings/firu_assets.dart';
import '../settings/custom_shared_preferences.dart';
import '../widgets/firu_logo.dart';
import '../widgets/colored_safe_area.dart';
import '../widgets/contact_icon.dart';

class InfoScreen extends StatefulWidget {

  @override
  _InfoScreenState createState() => _InfoScreenState();
}

class _InfoScreenState extends State<InfoScreen> {
  static const _firuDescription = 'Firulife busca apoyar a organizaciones que velan por el bienestar '+
                                  'de peluditos sin hogar. Igualmente permite a los usuarios encontrar '+
                                  'negocios relacionados a nuestros peluditos en casa, tales como '+
                                  'veterinarias, venta de juguetes y alimentos, entre otros.';
  String _version;
  String _country;

  @override
  void initState() {
    CustomSharedPreferences().getLocalCountry().then((value) {
      setState(() {
        _country = value;
      });
    });
    _initPackageInfo();
    super.initState();
  }

  Future<void> _initPackageInfo() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    setState(() {
      _version = info.version;
    });
  }

  @override
  Widget build(BuildContext context) {
    return ColoredSafeArea(
      child: Scaffold(
         appBar: AppBar(
          centerTitle: true,
          title: Text('Sobre Firulife', style: Theme.of(context).textTheme.headline6),
          elevation: 0
        ),
        body: Container(
          color: CustomColors.mainGreen,
          child: Column(children: <Widget>[
            Expanded(
              flex: 2,
              child: FiruLogo(color: Colors.transparent, radius: 70.0, imageAsset: FiruAssets.firuLogoRoundFull)
            ),
            Expanded(
              flex: 3,
              child: Container(
                padding: EdgeInsets.all(15),
                alignment: Alignment.center,
                child: Scrollbar(
                  child: SingleChildScrollView(
                    child: Text(
                      _firuDescription,
                      style: const TextStyle(fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center
                    )
                  )
                )
              )
            ),
            Expanded(
              flex: 2,
              child: Container(
                alignment: Alignment.bottomCenter,
                child: Column(children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(top: 15),
                    decoration: BoxDecoration(
                      border: Border(bottom: BorderSide(
                        width: 2,
                        color: CustomColors.secondaryGreen
                      ))
                    ),
                    child: Text('Contáctanos')
                  ),
                  Padding(padding: EdgeInsets.only(top: 5.0)),
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(children: [
                      ContactIcon(
                        contact: 'whatsapp',
                        url: '+50576145496',
                        isPhone: true,
                        personalizedColor: CustomColors.secondaryGreen
                      ),
                      ContactIcon(
                        contact: 'instagram',
                        url: _country == 'NIC' ? 'https://www.instagram.com/firulife_app/' : 'https://www.instagram.com/firulifesv/',
                        isPhone: false,
                        personalizedColor: CustomColors.secondaryGreen
                      ),
                      ContactIcon(
                        contact: 'facebook',
                        url: _country == 'NIC' ? 'https://www.facebook.com/firulifeapp/' : 'https://www.facebook.com/firulifeappsv/',
                        isPhone: false,
                        personalizedColor: CustomColors.secondaryGreen
                      ),
                      ContactIcon(
                        contact: 'email',
                        url: 'firulife.dev@gmail.com',
                        isPhone: false,
                        personalizedColor: CustomColors.secondaryGreen
                      ),
                      ContactIcon(
                        contact: 'web',
                        url: 'https://firu.life',
                        isPhone: false,
                        personalizedColor: CustomColors.secondaryGreen
                      )
                    ])
                  )
                ])
              )
            ),
            Expanded(
              flex: 1,
              child: Container(
                alignment: Alignment.bottomCenter,
                padding: EdgeInsets.only(bottom: 10.0),
                child: Text(
                  'Versión: $_version',
                  style: const TextStyle(fontSize: 15, color: CustomColors.secondaryGreen, fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center
                )
              )
            )
          ])
        )
      )
    );
  }

}
