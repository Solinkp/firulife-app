import 'package:flutter/material.dart';

import '../../../settings/custom_colors.dart';
import '../../../settings/placeholder_images.dart';
import '../../../widgets/info_tile.dart';
import '../../../widgets/info_panel.dart';

class BusinessInfoScreen extends StatelessWidget {
  final String pictureUrl;
  final List<Map<String, String>> addresses;
  final List<String> phone;
  final List<Map<String, String>> openingHours;
  final List<String> services;
  final Map<String, List<Map<String, String>>> donationAccounts;
  final List<Map<String, String>> contact;
  final Color businessColor;

  BusinessInfoScreen({
    @required this.pictureUrl,
    @required this.addresses,
    @required this.phone,
    @required this.openingHours,
    @required this.services,
    @required this.donationAccounts,
    @required this.contact,
    @required this.businessColor
  });

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      Positioned(
        top: 0.0,
        child: Container(
          padding: EdgeInsets.only(bottom: 25.0, left: 10.0, right: 10.0),
          height: 200.0,
          width: MediaQuery.of(context).size.width,
          color: businessColor ?? CustomColors.mainGreen,
          child: FadeInImage(
            fit: BoxFit.contain,
            placeholder: const AssetImage(PlaceholderImages.placeholderBanner),
            image: NetworkImage(pictureUrl),
            imageErrorBuilder: (BuildContext context, Object exception, StackTrace stackTrace) {
              return Image.asset(PlaceholderImages.placeholderBanner);
            }
          )
        )
      ),
      Positioned(
        top: 180.0,
        bottom: 0.0,
        width: MediaQuery.of(context).size.width,
        child: Container(
          decoration: BoxDecoration(
            color: CustomColors.secondaryGreen,
            borderRadius: BorderRadius.only(topLeft: Radius.circular(20.0), topRight: Radius.circular(20.0)),
          ),
          child: ClipRRect(
            borderRadius: const BorderRadius.only(topLeft: Radius.circular(20), topRight: Radius.circular(20)),
            child: SingleChildScrollView(
              child: Container(
                child: Column(children: [
                  Column(
                    children: _buildAddressTiles()
                  ),
                  Column(
                    children: _buildPhoneTiles()
                  ),
                  _buildPanel('Horarios de Atención', 0, list: openingHours),
                  _buildPanel('Servicios', 1, list: services),
                  _buildPanel('Donaciones', 2, map: donationAccounts),
                  _buildPanel('Contáctanos', 3, list: contact)
                ])
              )
            )
          )
        )
      )
    ]);
  }

  List<Widget> _buildAddressTiles() {
    List<Widget> tiles = [];
    if(addresses != null && addresses.length > 0) {
      addresses.cast().forEach((address) => {
        tiles.add(InfoTile(icon: Icons.location_on, title: address['address'], value: address['coordinates'], option: 0, personalizedColor: businessColor)),
        tiles.add(Divider())
      });
    }
    return tiles;
  }

  List<Widget> _buildPhoneTiles() {
    List<Widget> tiles = [];
    if(phone != null && phone.length > 0) {
      phone.forEach((phone) => {
        tiles.add(InfoTile(icon: Icons.contact_phone, title: phone, value: phone, option: 1, personalizedColor: businessColor)),
        tiles.add(Divider())
      });
    }
    return tiles;
  }

  Widget _buildPanel(String title, int type, {List list = const [], Map map = const {}}) {
    if (list != null && list.length > 0 || map != null && map.length > 0) {
      return InfoPanel(title: title, list: list.cast(), map: map.cast(), type: type, personalizedColor: businessColor);
    } else {
      return Container();
    }
  }

}
