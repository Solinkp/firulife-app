import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:age/age.dart';

import '../../../models/pet.dart';
import '../../../settings/custom_colors.dart';
import '../../../settings/placeholder_images.dart';
import '../../../settings/date_formatter.dart';
import '../../../widgets/colored_safe_area.dart';

class ShelterPetDetailsScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final List<Object> _args = ModalRoute.of(context).settings.arguments;
    final Pet pet = _args[0];
    final Color businessColor = _args[1];
    final IconData genderIcon = pet.sex ? MdiIcons.genderMale : MdiIcons.genderFemale;
    final Color genderIconColor = pet.sex ? Colors.lightBlueAccent : Colors.pinkAccent;
    final String gender = pet.sex ? 'Macho' : 'Hembra';
    final String age = _getAge(pet.age);
    final String birthDate = DateFormatter.getFormattedDate(pet.age);
    String petPicture = '';

    if(pet.pictureUrl != null && pet.pictureUrl.length > 0) {
      petPicture = pet.pictureUrl;
    }

    return ColoredSafeArea(
      color: businessColor,
      child: Scaffold(
        backgroundColor: businessColor ?? CustomColors.mainGreen,
        appBar: AppBar(
          centerTitle: true,
          title: Text(pet.name, style: Theme.of(context).textTheme.headline6),
          backgroundColor: businessColor ?? CustomColors.mainGreen,
          elevation: 0
        ),
        body: Stack(children: [
          Positioned(
            top: 0.0,
            child: Hero(
              tag: 'firu_${pet.id}',
              child: Container(
                padding: const EdgeInsets.only(bottom: 5.0, left: 10.0, right: 10.0),
                height: 300.0,
                width: MediaQuery.of(context).size.width,
                alignment: Alignment.center,
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(22),
                    border: Border.all(color: CustomColors.secondaryGreen, width: 2)
                  ),
                  child: ClipRRect(
                    borderRadius: const BorderRadius.all(Radius.circular(20)),
                    child: Container(
                      child: FadeInImage(
                        fit: BoxFit.contain,
                        placeholder: const AssetImage(PlaceholderImages.placeholderFiru),
                        image: petPicture.isEmpty ? const AssetImage(PlaceholderImages.placeholderFiru) : NetworkImage(petPicture),
                        imageErrorBuilder: (BuildContext context, Object exception, StackTrace stackTrace) {
                          return Image.asset(PlaceholderImages.placeholderFiru);
                        }
                      )
                    )
                  )
                )
              )
            )
          ),
          Positioned(
            top: 300.0,
            bottom: 0.0,
            width: MediaQuery.of(context).size.width,
            child: Container(
              decoration: BoxDecoration(
                color: CustomColors.secondaryGreen,
                borderRadius: const BorderRadius.only(topLeft: Radius.circular(20.0), topRight: Radius.circular(20.0)),
              ),
              child: ClipRRect(
                borderRadius: const BorderRadius.only(topLeft: Radius.circular(20.0), topRight: Radius.circular(20.0)),
                child: SingleChildScrollView(
                  child: Column(children: [
                    Container(
                      padding: const EdgeInsets.symmetric(vertical: 5.0),
                      width: double.infinity,
                      alignment: Alignment.center,
                      child: Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(10),
                          child: Text('Género:', style: const TextStyle(fontSize: 22, color: CustomColors.mainGreen))
                        ),
                        Tooltip(
                          message: '$gender',
                          child: IconButton(
                            icon: Icon(genderIcon, color: genderIconColor),
                            onPressed: null,
                          )
                        )
                      ])
                    ),
                    Divider(),
                    Container(
                      padding: const EdgeInsets.symmetric(vertical: 5.0),
                      width: double.infinity,
                      alignment: Alignment.center,
                      child: Row(children: [
                        Expanded(
                          flex: 2,
                          child: Text('Edad:', textAlign: TextAlign.center, style: const TextStyle(fontSize: 22, color: CustomColors.mainGreen))
                        ),
                        Expanded(
                          flex: 3,
                          child: Text(age, style: const TextStyle(fontSize: 20, color: CustomColors.mainDark))
                        ),
                        Expanded(
                          flex: 1,
                          child: Tooltip(
                            message: '$birthDate',
                            child: Icon(MdiIcons.cakeVariant, color: genderIconColor)
                          )
                        )
                      ])
                    ),
                    Divider(),
                    Container(
                      width: double.infinity,
                      alignment: Alignment.center,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                        child: Text(pet.description, style: const TextStyle(fontSize: 20, color: CustomColors.mainDark))
                      )
                    )
                  ])
                )
              )
            )
          )
        ])
      )
    );
  }

  String _getAge(petBirthday) {
    String result, yOld, mOld, dOld = '';
    final DateTime currentDate = DateTime.now();
    AgeDuration age = Age.dateDifference(fromDate: petBirthday, toDate: currentDate);

    if(age.years == 1) {
      yOld = 'año';
    } else if(age.years == 0 || age.years > 1) {
      yOld = 'años';
    }
    if(age.months == 1) {
      mOld = 'mes';
    } else if(age.months == 0 || age.months > 1) {
      mOld = 'meses';
    }
    if(age.days == 1) {
      dOld = 'día';
    } else if (age.days == 0 || age.days > 1) {
      dOld = 'días';
    }

    result = '${age.years} $yOld, ${age.months} $mOld, ${age.days} $dOld';

    return result;
  }

}
