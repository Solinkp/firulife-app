import 'package:flutter/material.dart';

import '../../../models/product.dart';
import '../../../settings/custom_colors.dart';
import '../../../settings/placeholder_images.dart';
import '../../../widgets/colored_safe_area.dart';

class BusinessProductDetailsScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final List<Object> _args = ModalRoute.of(context).settings.arguments;
    final Product product = _args[0];
    final Color businessColor = _args[1];
    String productPicture = '';

    if(product.pictureUrl != null && product.pictureUrl.length > 0) {
      productPicture = product.pictureUrl;
    }

    return ColoredSafeArea(
      color: businessColor,
      child: Scaffold(
        backgroundColor: businessColor ?? CustomColors.mainGreen,
        appBar: AppBar(
          centerTitle: true,
          title: Tooltip(
            message: product.name,
            child: Text(
              product.name,
              style: Theme.of(context).textTheme.headline4
            )
          ),
          backgroundColor: businessColor ?? CustomColors.mainGreen,
          elevation: 0
        ),
        body: Stack(children: [
          Positioned(
            top: 0.0,
            child: Hero(
              tag: 'product_${product.id}',
              child: Container(
                padding: const EdgeInsets.only(bottom: 5.0, left: 10.0, right: 10.0),
                height: 300.0,
                width: MediaQuery.of(context).size.width,
                alignment: Alignment.center,
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(22),
                    border: Border.all(color: CustomColors.secondaryGreen, width: 2)
                  ),
                  child: ClipRRect(
                    borderRadius: const BorderRadius.all(Radius.circular(20)),
                    child: Container(
                      child: FadeInImage(
                        fit: BoxFit.contain,
                        placeholder: const AssetImage(PlaceholderImages.placeholderProduct),
                        image: productPicture.isEmpty ? const AssetImage(PlaceholderImages.placeholderProduct) : NetworkImage(productPicture),
                        imageErrorBuilder: (BuildContext context, Object exception, StackTrace stackTrace) {
                          return Image.asset(PlaceholderImages.placeholderProduct);
                        }
                      )
                    )
                  )
                )
              )
            )
          ),
          Positioned(
            top: 300.0,
            bottom: 0.0,
            width: MediaQuery.of(context).size.width,
            child: Container(
              decoration: BoxDecoration(
                color: CustomColors.secondaryGreen,
                borderRadius: BorderRadius.only(topLeft: Radius.circular(20.0), topRight: Radius.circular(20.0)),
              ),
              child: ClipRRect(
                borderRadius: const BorderRadius.only(topLeft: Radius.circular(20.0), topRight: Radius.circular(20.0)),
                child: Column(children: [
                  Container(
                    padding: const EdgeInsets.symmetric(vertical: 5.0),
                    width: double.infinity,
                    alignment: Alignment.center,
                    child: Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(10),
                        child: Text('Precio:', style: const TextStyle(fontSize: 22, color: CustomColors.mainGreen))
                      ),
                      Padding(
                        padding: const EdgeInsets.all(10),
                        child: Text('${product.currency}${product.price}', style: const TextStyle(fontSize: 20, color: CustomColors.mainDark))
                      )
                    ])
                  ),
                  Divider(),
                  Expanded(
                    child: SingleChildScrollView(
                      child: Column(children: [
                        Container(
                          width: double.infinity,
                          alignment: Alignment.center,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
                            child: Text(product.fullDescription, style: const TextStyle(fontSize: 20, color: CustomColors.mainDark))
                          )
                        )
                      ])
                    )
                  )
                ])
              )
            )
          )
        ])
      )
    );
  }

}
