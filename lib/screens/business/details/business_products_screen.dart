import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../models/product.dart';
import '../../../settings/custom_colors.dart';
import '../../../widgets/spinner_loader.dart';
import '../../../widgets/business_product.dart';

class BusinessProductsScreen extends StatelessWidget {
  final Color businessColor;

  BusinessProductsScreen({@required this.businessColor});

  @override
  Widget build(BuildContext context) {
    List<Product> productsList = Provider.of<List<Product>>(context);

    return productsList == null ? SpinnerLoader(
      color: businessColor ?? CustomColors.mainGreen
    ) : productsList.length == 0 ? Container(
      alignment: Alignment.center,
      child: Text(
        'No hay productos disponibles por el momento.',
        style: TextStyle(fontSize: 23, color: CustomColors.mainDark),
        textAlign: TextAlign.center
      )
    ) : ListView.builder(
      itemCount: productsList.length,
      itemBuilder: (_, i) {
        return BusinessProduct(product: productsList[i], businessColor: businessColor);
      }
    );
  }

}
