import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';

import '../../../models/business.dart';
import '../../../models/pet.dart';
import '../../../models/product.dart';
import '../../../services/database_operation.dart';
import '../../../settings/custom_colors.dart';
import '../../../widgets/colored_safe_area.dart';
import '../../../widgets/custom_text_button.dart';
import 'business_info_screen.dart';
import 'shelter_pets_screen.dart';
import 'business_products_screen.dart';

class BusinessScreen extends StatefulWidget {

  @override
  _BusinessScreenState createState() => _BusinessScreenState();
}

class _BusinessScreenState extends State<BusinessScreen> {
  final BaseDatabaseOperation _database = DatabaseOperation();
  final _bottomNavigationKey = GlobalKey<CurvedNavigationBarState>();
  int _page = 0;
  String _title = 'Información';

  @override
  Widget build(BuildContext context) {
    final Business business = ModalRoute.of(context).settings.arguments;

    return ColoredSafeArea(
      color: business.businessColor,
      child: GestureDetector(
        onHorizontalDragEnd: (val) => _onHorizontalDrag(val, business.type),
        child: Scaffold(
          backgroundColor: CustomColors.secondaryGreen,
          appBar: AppBar(
            centerTitle: true,
            title: Text('$_title', style: Theme.of(context).textTheme.headline5),
            backgroundColor: business.businessColor ?? CustomColors.mainGreen,
            actions: business.type == 0 && _page == 1 ? [
              IconButton(
                icon: Icon(Icons.fact_check),
                onPressed: () => _openAdoptionInfo(business.name, business.adoptionLink)
              )
            ] : null,
            elevation: _page == 0 ? 0 : 4
          ),
          bottomNavigationBar: CurvedNavigationBar(
            key: _bottomNavigationKey,
            index: 0,
            color: business.businessColor ?? CustomColors.mainGreen,
            backgroundColor: CustomColors.secondaryGreen,
            buttonBackgroundColor: business.businessColor ?? CustomColors.mainGreen,
            items: _getTabs(business.type),
            height: 50.0,
            onTap: (index) => _swapPage(business.type, index)
          ),
          body: MultiProvider(
            providers: [
              StreamProvider<List<Pet>>.value(
                value: _database.getShelterPets(business.id),
                initialData: null,
                updateShouldNotify: (_, __) => true,
                catchError: (_, object) => null
              ),
              StreamProvider<List<Product>>.value(
                value: _database.getBusinessProducts(business.id),
                initialData: null,
                updateShouldNotify: (_, __) => true,
                catchError: (_, object) => null
              )
            ],
            child: _getBody(business)
          )
        )
      )
    );
  }

  List<Widget> _getTabs(int businessType) {
    List<Widget> iconList = [];

    if(businessType == 0) {
      iconList.add(Icon(Icons.description, color: CustomColors.secondaryGreen));
      iconList.add(Icon(Icons.pets, color: CustomColors.secondaryGreen));
      iconList.add(Icon(Icons.shopping_basket, color: CustomColors.secondaryGreen));
    }
    else if(businessType == 1 || businessType == 2) {
      iconList.add(Icon(Icons.description, color: CustomColors.secondaryGreen));
      iconList.add(Icon(Icons.shopping_basket, color: CustomColors.secondaryGreen));
    }
    else {
      iconList.add(Icon(Icons.description, color: CustomColors.secondaryGreen));
    }
    return iconList;
  }

  void _swapPage(int businessType, int index) {
    if(businessType == 0) {
      setState(() {
        _page = index;
        if(index == 0) {
          _title = 'Información';
        } else if(index == 1) {
          _title = 'Firus';
        } else {
          _title = 'Productos';
        }
      });
    }
    else if(businessType == 1 || businessType == 2) {
      setState(() {
        _page = index;
        if(index == 0) {
          _title = 'Información';
        } else {
          _title = 'Productos';
        }
      });
    }
  }

  Widget _getBody(Business business) {
    if(business.type == 0) {
      if(_page == 0) {
        return BusinessInfoScreen(
          pictureUrl: business.pictureUrl,
          addresses: business.addresses,
          phone: business.phone,
          openingHours: business.openingHours,
          services: business.services,
          donationAccounts: business.donationAccounts,
          contact: business.contact,
          businessColor: business.businessColor
        );
      } else if(_page == 1) {
        return ShelterPetsScreen(
          businessColor: business.businessColor
        );
      } else {
        return BusinessProductsScreen(
          businessColor: business.businessColor
        );
      }
    }
    else if(business.type == 1 || business.type == 2) {
      if(_page == 0) {
        return BusinessInfoScreen(
          pictureUrl: business.pictureUrl,
          addresses: business.addresses,
          phone: business.phone,
          openingHours: business.openingHours,
          services: business.services,
          donationAccounts: business.donationAccounts,
          contact: business.contact,
          businessColor: business.businessColor
        );
      } else {
        return BusinessProductsScreen(
          businessColor: business.businessColor
        );
      }
    }
    else {
      return BusinessInfoScreen(
        pictureUrl: business.pictureUrl,
        addresses: business.addresses,
        phone: business.phone,
        openingHours: business.openingHours,
        services: business.services,
        donationAccounts: business.donationAccounts,
        contact: business.contact,
        businessColor: business.businessColor
      );
    }
  }

  void _onHorizontalDrag(DragEndDetails val, int businessType) {
    double velX = val.velocity.pixelsPerSecond.dx.floorToDouble();
    double velY = val.velocity.pixelsPerSecond.dy.floorToDouble();

    if(businessType == 0) {
      if(velX.floor() < velY) {
        if(_page == 0) {
          _bottomNavigationKey.currentState.setPage(1);
        } else if(_page == 1) {
          _bottomNavigationKey.currentState.setPage(2);
        }
      }
      else if(velX.floor() > velY) {
        if(_page == 2) {
          _bottomNavigationKey.currentState.setPage(1);
        } else if(_page == 1) {
          _bottomNavigationKey.currentState.setPage(0);
        }
      }
    }
    else if(businessType == 1 || businessType == 2) {
      if(velX.floor() < velY) {
        if(_page == 0) {
          _bottomNavigationKey.currentState.setPage(1);
        }
      }
      else if(velX.floor() > velY) {
        if(_page == 1) {
          _bottomNavigationKey.currentState.setPage(0);
        }
      }
    }
  }

  void _openAdoptionInfo(String shelterName, String adoptionLink) {
    final String message = 'Recuerde que tener una mascota es una gran responsabilidad '+
                          'de por vida, que requiere gastos, cuidos y sobre todo tiempo, '+
                          'es como tener un hijo.\n\nPor lo que se pide que valore todos los '+
                          'requisitos que $shelterName propone, ya que la vida de una mascota, '+
                          'dependerá de usted.';
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: CustomColors.mainDark,
          content: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: TextStyle(fontSize: 16, color: Colors.white),
              children: <TextSpan>[
                TextSpan(text: '¡Aviso Importante!\n\n', style: TextStyle(fontWeight: FontWeight.bold)),
                TextSpan(text: message)
              ]
            )
          ),
          actions: [
            CustomTextButton(
              text: 'Cancelar',
              pressCallback: () => Navigator.of(context).pop()
            ),
            CustomTextButton(
              text: 'Solicitar Requisitos',
              pressCallback: () {
                if(adoptionLink != null && adoptionLink.length > 1) {
                  _openAdoptionForm(adoptionLink);
                } else {
                  Navigator.of(context).pop();
                }
              }
            )
          ]
        );
      }
    );
  }

  void _openAdoptionForm(String link) async {
    if (await canLaunch(link)) {
      await launch(link);
    } else {
      link = 'mailto:$link';
      if (await canLaunch(link)) {
        await launch(link);
      }
    }
  }

}
