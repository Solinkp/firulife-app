import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../models/pet.dart';
import '../../../settings/custom_colors.dart';
import '../../../widgets/spinner_loader.dart';
import '../../../widgets/shelter_pet.dart';

class ShelterPetsScreen extends StatelessWidget {
  final Color businessColor;

  ShelterPetsScreen({@required this.businessColor});

  @override
  Widget build(BuildContext context) {
    List<Pet> petsList = Provider.of<List<Pet>>(context);

    return petsList == null ? SpinnerLoader(
      color: businessColor ?? CustomColors.mainGreen
    ) : petsList.length == 0 ? Container(
      alignment: Alignment.center,
      child: Text(
        'No hay mascotas en adopción por el momento.',
        style: TextStyle(fontSize: 23, color: CustomColors.mainDark),
        textAlign: TextAlign.center
      )
    ) : GridView.builder(
      padding: EdgeInsets.all(10),
      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: 150,
        childAspectRatio: 4/6,
        crossAxisSpacing: 15,
        mainAxisSpacing: 20
      ),
      itemCount: petsList.length,
      itemBuilder: (_, i) {
        return ShelterPet(pet: petsList[i], businessColor: businessColor);
      }
    );
  }

}
