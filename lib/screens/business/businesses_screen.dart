import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:responsive_grid/responsive_grid.dart';

import '../../models/business.dart';
import '../../settings/custom_shared_preferences.dart';
import '../../settings/custom_colors.dart';
import '../../widgets/colored_safe_area.dart';
import '../../widgets/spinner_loader.dart';
import '../../widgets/firu_business.dart';

class BusinessesScreen extends StatefulWidget {

  @override
  _BusinessesScreenState createState() => _BusinessesScreenState();
}

class _BusinessesScreenState extends State<BusinessesScreen> {
  String _country;

  @override
  void initState() {
    CustomSharedPreferences().getLocalCountry().then((value) {
      setState(() {
        _country = value;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    int _businessType = ModalRoute.of(context).settings.arguments;
    String _title;

    switch(_businessType) {
      case 0:
        _title = "Firu Hogares";
        break;
      case 1:
        _title = "Firu Salud - Clínicas";
        break;
      case 2:
        _title = "Firu Tiendas";
        break;
      case 3:
        _title = "Firu Cuidadores";
        break;
      case 4:
        _title = "Firu Salud - Independientes";
        break;
    }

    return ColoredSafeArea(
      child: Scaffold(
        backgroundColor: CustomColors.secondaryGreen,
        appBar: AppBar(
          centerTitle: true,
          title: Text('$_title', style: Theme.of(context).textTheme.headline6)
        ),
        body: StreamBuilder(
          stream: FirebaseFirestore.instance.collection('business').where('countryId', isEqualTo: _country).where('type', isEqualTo: _businessType).where('active', isEqualTo: true).snapshots(),
          builder: (_, snapshot) {
            switch(snapshot.connectionState) {
              case ConnectionState.waiting:
                return SpinnerLoader(color: CustomColors.mainGreen);
                break;
              default:
                int businessCount = snapshot.data.docs.length;
                return businessCount == 0 ? Container(
                  alignment: Alignment.center,
                  child: Text(
                    'No hay registros en este segmento por el momento.',
                    style: TextStyle(fontSize: 23, color: CustomColors.mainDark),
                    textAlign: TextAlign.center
                  )
                ) : ResponsiveGridList(
                  rowMainAxisAlignment: MainAxisAlignment.center,
                  minSpacing: 20,
                  desiredItemWidth: 150,
                  children: _getBusiness(snapshot.data.docs)
                );
            }
          }
        )
      )
    );
  }

  List<Widget> _getBusiness(dynamic data) {
    List<Widget> businessList = [];
    for(var i = 0; i < data.length; i++) {
      final String documentId = data[i].id;
      Business business = Business.buildBusiness(documentId, data[i].data());
      Widget businessWidget = FiruBusiness(business: business);
      businessList.add(businessWidget);
    }
    return businessList;
  }

}
