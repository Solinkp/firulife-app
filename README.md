# firulife_app
Firulife Mobile App Project.

Some of the tech used:
- Flutter v1.22.6
- Firebase (firestore, storage, auth, functions)
- Material Icons

Description:
App destined to help animal shelters and give a platform to all pet related stuff, as well as a mini social network for posting lost or found pets. More functionalities to come...